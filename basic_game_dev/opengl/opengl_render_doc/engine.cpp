#include "engine.h"
#include "glad/glad.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <SDL2/SDL.h>

#define OM_GL_CHECK()                                                          \
  {                                                                            \
    const int err = static_cast<int>(glGetError());                            \
    if (err != GL_NO_ERROR) {                                                  \
      switch (err) {                                                           \
      case GL_INVALID_ENUM:                                                    \
        std::cerr << "GL_INVALID_ENUM" << std::endl;                           \
        break;                                                                 \
      case GL_INVALID_VALUE:                                                   \
        std::cerr << "GL_INVALID_VALUE" << std::endl;                          \
        break;                                                                 \
      case GL_INVALID_OPERATION:                                               \
        std::cerr << "GL_INVALID_OPERATION" << std::endl;                      \
        break;                                                                 \
      case GL_INVALID_FRAMEBUFFER_OPERATION:                                   \
        std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;          \
        break;                                                                 \
      case GL_OUT_OF_MEMORY:                                                   \
        std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                          \
        break;                                                                 \
      }                                                                        \
      assert(false);                                                           \
    }                                                                          \
  }

namespace engine {

static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar *message, [[maybe_unused]] const void *userParam);

static std::array<std::string_view, 15> event_names{
    {"UP_pressed", "UP_released", "LEFT_pressed", "LEFT_released",
     "DOWN_pressed", "DOWN_released", "RIGHT_pressed", "RIGHT_released",
     "FIRE_pressed", "FIRE_released", "PAUSE_pressed", "PAUSE_released",
     "PLAY_pressed", "PLAY_released", "TURN_off"}};

std::ostream &operator<<(std::ostream &stream, const event e) {
  std::uint32_t value = static_cast<std::uint32_t>(e);
  std::uint32_t minimal = static_cast<std::uint32_t>(event::up_pressed);
  std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
  if (value >= minimal && value <= maximal) {
    stream << event_names[value];
    return stream;
  } else {
    throw std::runtime_error("Too big event value");
  }
}

struct bind_keyboard {
  SDL_Keycode key;
  std::string_view name;
  event event_pressed;
  event event_released;
};

const std::array<bind_keyboard, 8> keys{
    {{SDLK_w, "UP", event::up_pressed, event::up_released},
     {SDLK_a, "LEFT", event::left_pressed, event::left_released},
     {SDLK_s, "DOWN", event::dowm_pressed, event::down_released},
     {SDLK_d, "RIGHT", event::right_pressed, event::right_released},
     {SDLK_SPACE, "FIRE", event::fire_pressed, event::fire_released},
     {SDLK_ESCAPE, "PAUSE", event::pause_pressed, event::pause_released},
     {SDLK_KP_ENTER, "PLAY", event::play_pressed, event::play_released}}};
static bool check_input_keyboard_event(const SDL_Event &e,
                                       const bind_keyboard *&result) {
  using namespace std;
  const auto it_for_keyboard =
      find_if(begin(keys), end(keys), [&](const bind_keyboard &b) {
        return b.key == e.key.keysym.sym;
      });
  if (it_for_keyboard != end(keys)) {
    result = &(*it_for_keyboard);
    return true;
  }
  return false;
}

struct bind_controller {
  SDL_GameControllerButton button;
  std::string_view name;
  event event_pressed;
  event event_released;
};

const std::array<bind_controller, 8> controller_buttons{
    {{SDL_CONTROLLER_BUTTON_DPAD_UP, "UP", event::up_pressed,
      event::up_released},
     {SDL_CONTROLLER_BUTTON_DPAD_LEFT, "LEFT", event::left_pressed,
      event::left_released},
     {SDL_CONTROLLER_BUTTON_DPAD_DOWN, "DOWN", event::dowm_pressed,
      event::down_released},
     {SDL_CONTROLLER_BUTTON_DPAD_RIGHT, "RIGHT", event::right_pressed,
      event::right_released},
     {SDL_CONTROLLER_BUTTON_X, "FIRE", event::fire_pressed,
      event::fire_released},
     {SDL_CONTROLLER_BUTTON_A, "PAUSE", event::pause_pressed,
      event::pause_released},
     {SDL_CONTROLLER_BUTTON_B, "PLAY", event::play_pressed,
      event::play_released}}};

static bool check_input_controller_event(const SDL_Event &e,
                                         const bind_controller *&result) {
  using namespace std;
  const auto it_for_controller = find_if(
      begin(controller_buttons), end(controller_buttons),
      [&](const bind_controller &b) { return b.button == e.cbutton.state; });
  if (it_for_controller != end(controller_buttons)) {
    result = &(*it_for_controller);
    return true;
  }
  return false;
}

std::istream &operator>>(std::istream &is, vertex &v) {
  is >> v.x;
  is >> v.y;
  is >> v.z;

  is >> v.r;
  is >> v.g;
  is >> v.b;

  return is;
}

std::istream &operator>>(std::istream &is, triangle &tr) {
  is >> tr.v[0];
  is >> tr.v[1];
  is >> tr.v[2];
  return is;
}

class engine_impl final : public engine {
public:
  std::string initialize(std::string_view) final {
    using namespace std;
    stringstream serr;

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
      serr << "Unable to initialize SDL: " << SDL_GetError() << endl;
      return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    window =
        SDL_CreateWindow("An SDL2 window", SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_OPENGL);
    if (window == nullptr) {
      serr << "Could not create window: " << SDL_GetError() << endl;
      SDL_Quit();
      return serr.str();
    }

    context = SDL_GL_CreateContext(window);
    if (context == nullptr) {
      serr << "Could not create context: " << SDL_GetError() << endl;
      SDL_Quit();
      return serr.str();
    }

    //    int result =
    //        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,
    //        &gl_major_version);
    //    assert(result == 0);

    //    result =
    //        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,
    //        &gl_minor_version);
    //    assert(result == 0);

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) {
      std::clog << "error: failed to initialize glad" << std::endl;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(callback_opengl_debug, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr,
                          GL_TRUE);

    SDL_GameController *controller = nullptr;
    for (int i = 0; i < SDL_NumJoysticks(); ++i) {
      if (SDL_IsGameController(i)) {
        controller = SDL_GameControllerOpen(i);
        if (controller) {
          break;
        } else {
          fprintf(stderr, "Could not open gamcontoller %i: %s\n", i,
                  SDL_GetError());
        }
      }
    }

    /// RENDERR DOC
    GLuint vertex_buffer = 0;
    glGenBuffers(1, &vertex_buffer);
    OM_GL_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    OM_GL_CHECK()
    GLuint vertex_array_object = 0;
    glGenVertexArrays(1, &vertex_array_object);
    OM_GL_CHECK()
    glBindVertexArray(vertex_array_object);
    OM_GL_CHECK()

    GLuint vert_shader = glCreateShader(GL_VERTEX_SHADER);
    OM_GL_CHECK()
    string_view vertex_shader_src = R"(#version 330 core
                                    layout (location = 0) in vec3 a_position;
                                    layout (location = 1) in vec3 a_color;
                                    out vec4 v_position;
                                    out vec3 v_color;
                                    void main()
                                    {
                                        v_position = vec4(a_position, 1.0);
                                        v_color = a_color;
                                        gl_Position = v_position;
                                    }
                                    )";
    const char *source = vertex_shader_src.data();
    glShaderSource(vert_shader, 1, &source, nullptr);
    OM_GL_CHECK()

    glCompileShader(vert_shader);
    OM_GL_CHECK()

    GLint compiled_status = 0;
    glGetShaderiv(vert_shader, GL_COMPILE_STATUS, &compiled_status);
    OM_GL_CHECK()
    if (compiled_status == 0) {
      GLint info_len = 0;
      glGetShaderiv(vert_shader, GL_INFO_LOG_LENGTH, &info_len);
      OM_GL_CHECK()
      std::vector<char> info_chars(static_cast<size_t>(info_len));
      glGetShaderInfoLog(vert_shader, info_len, nullptr, info_chars.data());
      OM_GL_CHECK()
      glDeleteShader(vert_shader);
      OM_GL_CHECK()

      std::string shader_type_name = "vertex";
      serr << "Error compiling shader(vertex)\n"
           << vertex_shader_src << "\n"
           << info_chars.data();
      return serr.str();
    }

    // create fragment shader

    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    OM_GL_CHECK()
    string_view fragment_shader_src = R"(#version 330 core
                                      in vec4 v_position;
                                      in vec3 v_color;
                                      out vec4 FragColor;
                                      void main()
                                      {
                                          FragColor = vec4(v_color, 1.0);
                                          /*
                                          if (v_position.z >= 0.0)
                                          {
                                            float light_green = 0.5 + v_position.z / 2.0;
                                            FragColor = vec4(0.0, light_green, 0.0, 1.0);
                                          } else
                                          {
                                            float dark_green = 0.5 - (v_position.z / -2.0);
                                            FragColor = vec4(0.0, dark_green, 0.0, 1.0);
                                          }
                                          */
                                      }
                                      )";
    source = fragment_shader_src.data();
    glShaderSource(fragment_shader, 1, &source, nullptr);
    OM_GL_CHECK()

    glCompileShader(fragment_shader);
    OM_GL_CHECK()

    compiled_status = 0;
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled_status);
    OM_GL_CHECK()
    if (compiled_status == 0) {
      GLint info_len = 0;
      glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_len);
      OM_GL_CHECK()
      std::vector<char> info_chars(static_cast<size_t>(info_len));
      glGetShaderInfoLog(fragment_shader, info_len, nullptr, info_chars.data());
      OM_GL_CHECK()
      glDeleteShader(fragment_shader);
      OM_GL_CHECK()

      serr << "Error compiling shader(fragment)\n"
           << vertex_shader_src << "\n"
           << info_chars.data();
      return serr.str();
    }

    // now create program and attach vertex and fragment shaders

    program_id_ = glCreateProgram();
    OM_GL_CHECK()
    if (0 == program_id_) {
      serr << "failed to create gl program";
      return serr.str();
    }

    glAttachShader(program_id_, vert_shader);
    OM_GL_CHECK()
    glAttachShader(program_id_, fragment_shader);
    OM_GL_CHECK()

    // bind attribute location
    glBindAttribLocation(program_id_, 0, "a_position");
    OM_GL_CHECK()
    glBindAttribLocation(program_id_, 1, "a_color");
    OM_GL_CHECK()
    // link program after binding attribute locations
    glLinkProgram(program_id_);
    OM_GL_CHECK()
    // Check the link status
    GLint linked_status = 0;
    glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
    OM_GL_CHECK()
    if (linked_status == 0) {
      GLint infoLen = 0;
      glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
      OM_GL_CHECK()
      std::vector<char> infoLog(static_cast<size_t>(infoLen));
      glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
      OM_GL_CHECK()
      serr << "Error linking program:\n" << infoLog.data();
      glDeleteProgram(program_id_);
      OM_GL_CHECK()
      return serr.str();
    }

    // turn on rendering with just created shader program
    glUseProgram(program_id_);
    OM_GL_CHECK()

    glEnable(GL_DEPTH_TEST);
    OM_GL_CHECK()
    return "";
  }

  void render_triangle(const triangle &t) final {
    glBufferData(GL_ARRAY_BUFFER, sizeof(t), &t, GL_STATIC_DRAW);
    OM_GL_CHECK()
    glEnableVertexAttribArray(0);

    GLintptr position_attr_offset = 0;

    OM_GL_CHECK()
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void *>(position_attr_offset));
    OM_GL_CHECK()
    glEnableVertexAttribArray(1);
    OM_GL_CHECK()

    GLintptr color_attr_offset = sizeof(float) * 3;

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void *>(color_attr_offset));
    OM_GL_CHECK()
    glValidateProgram(program_id_);
    OM_GL_CHECK()
    // Check the validate status
    GLint validate_status = 0;
    glGetProgramiv(program_id_, GL_VALIDATE_STATUS, &validate_status);
    OM_GL_CHECK()
    if (validate_status == GL_FALSE) {
      GLint infoLen = 0;
      glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
      OM_GL_CHECK()
      std::vector<char> infoLog(static_cast<size_t>(infoLen));
      glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
      OM_GL_CHECK()
      std::cerr << "Error linking program:\n" << infoLog.data();
      throw std::runtime_error("error");
    }
    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK()
  }

  void swap_buffers() final {
    SDL_GL_SwapWindow(window);

    glClearColor(0.3f, 0.3f, 1.0f, 0.0f);
    OM_GL_CHECK()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    OM_GL_CHECK()
  }

  bool read_inrut(event &e) final {
    using namespace std;
    SDL_Event sdl_event;
    if (SDL_PollEvent(&sdl_event)) {
      const bind_keyboard *binding_keyboard = nullptr;
      const bind_controller *binding_controller = nullptr;
      if (sdl_event.type == SDL_QUIT) {
        e = event::turn_off;
        return true;
      } else if (sdl_event.type == SDL_KEYDOWN) {
        if (check_input_keyboard_event(sdl_event, binding_keyboard)) {
          e = binding_keyboard->event_pressed;
          return true;
        }
      } else if (sdl_event.type == SDL_KEYUP) {
        if (check_input_keyboard_event(sdl_event, binding_keyboard)) {
          e = binding_keyboard->event_released;
          return true;
        }
      } else if (sdl_event.type == SDL_CONTROLLERDEVICEADDED) {
        cerr << "Controller added" << endl;
      } else if (sdl_event.type == SDL_CONTROLLERDEVICEREMOVED) {
        cerr << "Controller removed" << endl;
      } else if (sdl_event.type == SDL_CONTROLLERBUTTONDOWN) {
        if (check_input_controller_event(sdl_event, binding_controller)) {
          e = binding_controller->event_pressed;
          return true;
        }
      } else if (sdl_event.type == SDL_CONTROLLERBUTTONUP) {
        if (check_input_controller_event(sdl_event, binding_controller)) {
          e = binding_controller->event_released;
          return true;
        }
      }
    }
    return false;
  }
  void uninitialize() final {
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();
  }

private:
  SDL_Window *window = nullptr;
  SDL_GLContext context = nullptr;
  GLuint program_id_ = 0;
};

static bool already_exist = false;
engine *create_engine() {
  if (already_exist) {
    throw std::runtime_error("Engine already exist");
  }
  engine *result = new engine_impl();
  already_exist = true;
  return result;
}

void destroy_engine(engine *eng) {
  if (already_exist == false) {
    throw std::runtime_error("Engine didn't create");
  }
  if (eng == nullptr) {
    throw std::runtime_error("eng is nullptr");
  }
  delete eng;
}

engine::~engine() {}

static const char *source_to_strv(GLenum source) {
  switch (source) {
  case GL_DEBUG_SOURCE_API:
    return "API";
  case GL_DEBUG_SOURCE_SHADER_COMPILER:
    return "SHADER_COMPILER";
  case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
    return "WINDOW_SYSTEM";
  case GL_DEBUG_SOURCE_THIRD_PARTY:
    return "THIRD_PARTY";
  case GL_DEBUG_SOURCE_APPLICATION:
    return "APPLICATION";
  case GL_DEBUG_SOURCE_OTHER:
    return "OTHER";
  }
  return "unknown";
}

static const char *type_to_strv(GLenum type) {
  switch (type) {
  case GL_DEBUG_TYPE_ERROR:
    return "ERROR";
  case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
    return "DEPRECATED_BEHAVIOR";
  case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
    return "UNDEFINED_BEHAVIOR";
  case GL_DEBUG_TYPE_PERFORMANCE:
    return "PERFORMANCE";
  case GL_DEBUG_TYPE_PORTABILITY:
    return "PORTABILITY";
  case GL_DEBUG_TYPE_MARKER:
    return "MARKER";
  case GL_DEBUG_TYPE_PUSH_GROUP:
    return "PUSH_GROUP";
  case GL_DEBUG_TYPE_POP_GROUP:
    return "POP_GROUP";
  case GL_DEBUG_TYPE_OTHER:
    return "OTHER";
  }
  return "unknown";
}

static const char *severity_to_strv(GLenum severity) {
  switch (severity) {
  case GL_DEBUG_SEVERITY_HIGH:
    return "HIGH";
  case GL_DEBUG_SEVERITY_MEDIUM:
    return "MEDIUM";
  case GL_DEBUG_SEVERITY_LOW:
    return "LOW";
  case GL_DEBUG_SEVERITY_NOTIFICATION:
    return "NOTIFICATION";
  }
  return "unknown";
}

// 30Kb on my system, too much for stack
static std::array<char, GL_MAX_DEBUG_MESSAGE_LENGTH> local_log_buff;

static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar *message, [[maybe_unused]] const void *userParam) {
  // The memory formessageis owned and managed by the GL, and should onlybe
  // considered valid for the duration of the function call.The behavior of
  // calling any GL or window system function from within thecallback function
  // is undefined and may lead to program termination.Care must also be taken
  // in securing debug callbacks for use with asynchronousdebug output by
  // multi-threaded GL implementations.  Section 18.8 describes thisin further
  // detail.

  auto &buff{local_log_buff};
  int num_chars = std::snprintf(buff.data(), buff.size(), "%s %s %d %s %.*s\n",
                                source_to_strv(source), type_to_strv(type), id,
                                severity_to_strv(severity), length, message);

  if (num_chars > 0) {
    // TODO use https://en.cppreference.com/w/cpp/io/basic_osyncstream
    // to fix possible data races
    // now we use GL_DEBUG_OUTPUT_SYNCHRONOUS to garantie call in main
    // thread
    std::cerr.write(buff.data(), num_chars);
  }
}

} // namespace engine
