#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>

#include "engine.h"

std::vector<engine::line> generate_grid();

int main(int, char **, char **) {
  using namespace std;

  std::unique_ptr<engine::engine, void (*)(engine::engine *)> engine(
      engine::create_engine(), engine::destroy_engine);

  engine->initialize("");

  bool continue_loop = true;

  vector<engine::line> line_buff = generate_grid();
  cout << line_buff.size() * sizeof(engine::line);
  while (continue_loop) {
    engine::event event;
    while (engine->read_inrut(event)) {
      cout << event << endl;
      switch (event) {
      case engine::event::turn_off:
        continue_loop = false;
        break;
      default:
        break;
      }
    }
    //    std::ifstream file("vertexes.txt");
    //    assert(!!file);

    //    engine::line tr;
    //    file >> tr;
    engine->render_grid(line_buff);

    engine->swap_buffers();
  }
  engine->uninitialize();
  return EXIT_SUCCESS;
}

std::vector<engine::line> generate_grid() {
  const int width = 640;
  const int height = 480;

  const int x_count = 10;
  const int y_count = 10;

  const int x_step = width / x_count;
  const int y_step = height / y_count;

  engine::vertex vertex_;
  engine::line line_;
  std::vector<engine::vertex> vertex_buffer;
  std::vector<engine::line> line_buffer;

  for (int x = 0; x <= x_count; ++x) {
    for (int y = 0; y <= y_count; ++y) {
      vertex_.x = x * x_step / 320.f - 1;
      vertex_.y = y * y_step / 240.f - 1;
      vertex_.z = 0;
      vertex_.r = 0;
      vertex_.g = 1;
      vertex_.b = 0;
      vertex_buffer.push_back(vertex_);
    }
  }

  for (int x = 0; x <= x_count; ++x) {
    for (int y = 0; y <= y_count; ++y) {
      if (y == y_count) {
        line_.v[0] = vertex_buffer.at(x * 11 + y);
        line_.v[1] = vertex_buffer.at(x * 11 + y);
        line_buffer.push_back(line_);
      } else {
        line_.v[0] = vertex_buffer.at(x * 11 + y);
        line_.v[1] = vertex_buffer.at(x * 11 + y + 1);
        line_buffer.push_back(line_);
      }
    }
  }

  for (int y = 0; y <= y_count; ++y) {
    for (int x = 0; x <= x_count; ++x) {
      if (x == x_count) {
        line_.v[0] = vertex_buffer.at(y + x * 11);
        line_.v[1] = vertex_buffer.at(y + x * 11);
        line_buffer.push_back(line_);
      } else {
        line_.v[0] = vertex_buffer.at(y + x * 11);
        line_.v[1] = vertex_buffer.at(y + (x + 1) * 11);
        line_buffer.push_back(line_);
      }
    }
  }
  return line_buffer;
}
