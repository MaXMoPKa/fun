#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>

#include "engine.h"

int main(int, char **, char **) {
  using namespace std;

  std::unique_ptr<engine::engine, void (*)(engine::engine *)> engine(
      engine::create_engine(), engine::destroy_engine);

  engine->initialize("");

  bool continue_loop = true;
  while (continue_loop) {
    engine::event event;
    while (engine->read_inrut(event)) {
      cout << event << endl;
      switch (event) {
      case engine::event::turn_off:
        continue_loop = false;
        break;
      default:
        break;
      }
    }
    std::ifstream file("vertexes.txt");
    assert(!!file);

    engine::triangle tr;
    file >> tr;
    engine->render_triangle(tr);

    file >> tr;
    engine->render_triangle(tr);

    engine->swap_buffers();
  }
  engine->uninitialize();
  return EXIT_SUCCESS;
}
