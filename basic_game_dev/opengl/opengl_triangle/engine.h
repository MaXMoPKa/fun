#include <iosfwd>
#include <string>
#include <string_view>

#ifndef ENGINE_DECLSPEC
#define ENGINE_DECLSPEC
#endif

namespace engine {
enum class event {
  up_pressed,
  up_released,
  left_pressed,
  left_released,
  dowm_pressed,
  down_released,
  right_pressed,
  right_released,
  fire_pressed,
  fire_released,
  pause_pressed,
  pause_released,
  play_pressed,
  play_released,
  turn_off
};
ENGINE_DECLSPEC std::ostream &operator<<(std::ostream &stream, const event e);

class engine;

ENGINE_DECLSPEC engine *create_engine();
ENGINE_DECLSPEC void destroy_engine(engine *eng);

struct vertex {
  vertex() : x(0.f), y(0.f), z(0.f) {}
  float x;
  float y;
  float z;
};

struct triangle {
  vertex v[3];
  triangle() {
    v[0] = vertex();
    v[1] = vertex();
    v[2] = vertex();
  }
};

std::istream &operator>>(std::istream &stream, triangle &);
std::istream &operator>>(std::istream &stream, vertex &);

class ENGINE_DECLSPEC engine {
public:
  virtual ~engine();
  virtual void uninitialize() = 0;
  virtual std::string initialize(std::string_view config) = 0;
  virtual bool read_inrut(event &e) = 0;

  virtual void render_triangle(const triangle &) = 0;
  virtual void swap_buffers() = 0;
};

} // namespace engine
