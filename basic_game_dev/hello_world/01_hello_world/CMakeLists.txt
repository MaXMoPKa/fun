cmake_minimum_required(VERSION 3.17)

project(01_hello_world CXX)

add_executable(01_hello_world_app main.cpp)

set(CMAKE_CXX_STANDARD 17)
