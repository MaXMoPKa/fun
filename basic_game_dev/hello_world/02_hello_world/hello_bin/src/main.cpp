#include <cstdlib>
#include <hello_lib.h>
int main(int argc, char** argv, char** env)
{
    const char* user_env_name = std::getenv("USER");
	std::string_view user = user_env_name != nullptr ? user_env_name : "USER - environment variable not found";
    bool ended_well = output(user);
	int result = ended_well ? EXIT_SUCCESS : EXIT_FAILURE;
	return result;
}
