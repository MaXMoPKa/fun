#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>

#include "engine.h"

engine::v0 blend(const engine::v0 &vl, const engine::v0 &vr, const float a) {
  engine::v0 r;
  r.pos.x = (1.0f - a) * vl.pos.x + a * vr.pos.x;
  r.pos.y = (1.0f - a) * vl.pos.y + a * vr.pos.y;
  return r;
}

engine::tr0 blend(const engine::tr0 &tl, const engine::tr0 &tr, const float a) {
  engine::tr0 r;
  r.v[0] = blend(tl.v[0], tr.v[0], a);
  r.v[1] = blend(tl.v[1], tr.v[1], a);
  r.v[2] = blend(tl.v[2], tr.v[2], a);
  return r;
}

int main(int, char **, char **) {
  using namespace std;

  std::unique_ptr<engine::engine, void (*)(engine::engine *)> engine(
      engine::create_engine(), engine::destroy_engine);

  engine->initialize("");
  //  const std::string error = engine->initialize("");
  //  if (!error.empty()) {
  //    std::cerr << error << std::endl;
  //    return EXIT_FAILURE;
  //  }

  engine::texture *texture = engine->create_texture("tank.png");
  if (nullptr == texture) {
    std::cerr << "failed load texture\n";
    return EXIT_FAILURE;
  }

  bool continue_loop = true;
  bool stop_rotate = false;
  float stop_time = 0.f;
  int current_shader = 0;
  while (continue_loop) {
    engine::event event;
    while (engine->read_inrut(event)) {
      cout << event << endl;
      switch (event) {
      case engine::event::turn_off:
        continue_loop = false;
        break;
      case engine::event::fire_released:
        ++current_shader;
        if (current_shader > 2) {
          current_shader = 0;
        }
        break;
      default:

        break;
      }
    }
    if (current_shader == 0) {
      std::ifstream file("vert_pos.txt");
      assert(!!file);

      engine::tr0 tr1;
      engine::tr0 tr2;
      engine::tr0 tr11;
      engine::tr0 tr22;

      file >> tr1 >> tr2 >> tr11 >> tr22;

      float time = engine->get_time_from_init();
      engine::mat2x3 move =
          engine::mat2x3::move(engine::vec2(std::sin(time), 0.f));

      engine::mat2x3 aspect;
      aspect.col0.x = 1;
      aspect.col0.y = 0.f;
      aspect.col1.x = 0.f;
      aspect.col1.y = 640.f / 480.f;

      engine::mat2x3 m =
          engine::mat2x3::rotation(std::sin(time)) * move * aspect;

      for (auto &v : tr1.v) {
        v.pos = v.pos * m;
      }
      for (auto &v : tr2.v) {
        v.pos = v.pos * m;
      }

      engine->render(tr1, engine::color(1.f, 0.f, 0.f, 1.f));
      engine->render(tr2, engine::color(0.f, 1.f, 0.f, 1.f));
    }

    if (current_shader == 1) {
      std::ifstream file("vert_pos_color.txt");
      assert(!!file);

      engine::tr1 tr1;
      engine::tr1 tr2;

      file >> tr1 >> tr2;

      engine->render(tr1);
      engine->render(tr2);
    }

    if (current_shader == 2) {
      std::ifstream file("vert_tex_color.txt");
      assert(!!file);

      engine::tr2 tr1;
      engine::tr2 tr2;

      file >> tr1 >> tr2;

      float time = engine->get_time_from_init();
      engine::mat2x3 move =
          engine::mat2x3::move(engine::vec2(std::sin(time), 0.f));

      engine::mat2x3 aspect;
      aspect.col0.x = 1;
      aspect.col0.y = 0.f;
      aspect.col1.x = 0.f;
      aspect.col1.y = 640.f / 480.f;

      engine::mat2x3 m =
          engine::mat2x3::rotation(std::sin(time)) * move * aspect;

      engine->render(tr1, texture, m);
      engine->render(tr2, texture, m);
    }

    engine->swap_buffers();
  }
  engine->uninitialize();
  return EXIT_SUCCESS;
}
