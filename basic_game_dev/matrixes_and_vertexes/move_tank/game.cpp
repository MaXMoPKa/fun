#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>

#include "engine.h"

int main(int, char **, char **) {
  using namespace std;

  std::unique_ptr<engine::engine, void (*)(engine::engine *)> engine(
      engine::create_engine(), engine::destroy_engine);

  const std::string error = engine->initialize("");
  if (!error.empty()) {
    std::cerr << error << std::endl;
    return EXIT_FAILURE;
  }

  engine::texture *texture = engine->create_texture("tank.png");
  if (nullptr == texture) {
    std::cerr << "failed load texture\n";
    return EXIT_FAILURE;
  }

  engine::vertex_buffer *vertex_buf = nullptr;

  std::ifstream file("vert_tex_color.txt");
  if (!file) {
    std::cerr << "can't load vert_tex_color.txt\n";
    return EXIT_FAILURE;
  } else {
    std::array<engine::tr2, 2> tr;
    file >> tr[0] >> tr[1];
    vertex_buf = engine->create_vertex_buffer(&tr[0], tr.size());
    if (vertex_buf == nullptr) {
      std::cerr << "can't create vertex buffer\n";
      return EXIT_FAILURE;
    }
  }

  bool continue_loop = true;
  engine::vec2 current_tank_pos(0.f, 0.f);
  float current_tank_direction(0.f);
  const float pi = 3.1415926f;
  while (continue_loop) {
    engine::event event;
    while (engine->read_inrut(event)) {
      cout << event << endl;
      switch (event) {
      case engine::event::turn_off:
        continue_loop = false;
        break;
      default:

        break;
      }
    }
    if (engine->is_key_down(engine::keys::left)) {
      current_tank_pos.x -= 0.01f;
      current_tank_direction = pi / 2.f;
    } else if (engine->is_key_down(engine::keys::right)) {
      current_tank_pos.x += 0.01f;
      current_tank_direction = -pi / 2.f;
    } else if (engine->is_key_down(engine::keys::up)) {
      current_tank_pos.y += 0.01f;
      current_tank_direction = -pi;
    } else if (engine->is_key_down(engine::keys::down)) {
      current_tank_pos.y -= 0.01f;
      current_tank_direction = 0.f;
    }

    engine::mat2x3 move = engine::mat2x3::move(current_tank_pos);
    engine::mat2x3 aspect = engine::mat2x3::scale(1, 640.f / 480.f);
    engine::mat2x3 rot = engine::mat2x3::rotation(current_tank_direction);
    engine::mat2x3 m = rot * move * aspect;

    engine->render(*vertex_buf, texture, m);
    engine->swap_buffers();
  }

  engine->uninitialize();
  return EXIT_SUCCESS;
}
