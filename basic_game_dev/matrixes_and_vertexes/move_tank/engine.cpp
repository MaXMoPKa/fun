#include "engine.h"
#include "glad/glad.h"
#include "picopng.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL_opengles2.h>

#define OM_GL_CHECK()                                                          \
  {                                                                            \
    const int err = static_cast<int>(glGetError());                            \
    if (err != GL_NO_ERROR) {                                                  \
      switch (err) {                                                           \
      case GL_INVALID_ENUM:                                                    \
        std::cerr << "GL_INVALID_ENUM" << std::endl;                           \
        break;                                                                 \
      case GL_INVALID_VALUE:                                                   \
        std::cerr << "GL_INVALID_VALUE" << std::endl;                          \
        break;                                                                 \
      case GL_INVALID_OPERATION:                                               \
        std::cerr << "GL_INVALID_OPERATION" << std::endl;                      \
        break;                                                                 \
      case GL_INVALID_FRAMEBUFFER_OPERATION:                                   \
        std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;          \
        break;                                                                 \
      case GL_OUT_OF_MEMORY:                                                   \
        std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                          \
        break;                                                                 \
      }                                                                        \
      assert(false);                                                           \
    }                                                                          \
  }

namespace engine {

static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar *message, [[maybe_unused]] const void *userParam);

vec2::vec2() : x(0.f), y(0.f) {}
vec2::vec2(float x_, float y_) : x(x_), y(y_) {}

vec2 operator+(const vec2 left, const vec2 right) {
  vec2 result;
  result.x = left.x + right.x;
  result.y = left.y + right.y;
  return result;
}

mat2x3::mat2x3() : col0(1.0f, 0.0f), col1(0.0f, 1.0f), delta(0.0f, 0.0f) {}

mat2x3 mat2x3::identiry() { return mat2x3::scale(1.0f); }

mat2x3 mat2x3::scale(float scale) {
  mat2x3 result;
  result.col0.x = scale;
  result.col1.y = scale;
  return result;
}

mat2x3 mat2x3::scale(float sx, float sy) {
  mat2x3 r;
  r.col0.x = sx;
  r.col1.y = sy;
  return r;
}

mat2x3 mat2x3::rotation(float thetha) {
  mat2x3 result;
  result.col0.x = std::cos(thetha);
  result.col0.y = std::sin(thetha);

  result.col1.x = -std::sin(thetha);
  result.col1.y = std::cos(thetha);

  return result;
}

mat2x3 mat2x3::move(const vec2 &delta) {
  mat2x3 r = mat2x3::identiry();
  r.delta = delta;
  return r;
}

vec2 operator*(const vec2 &v, const mat2x3 &m) {
  vec2 result;
  result.x = v.x * m.col0.x + v.y * m.col0.y + m.delta.x;
  result.y = v.x * m.col1.x + v.y * m.col1.y + m.delta.y;
  return result;
}

mat2x3 operator*(const mat2x3 &m1, const mat2x3 &m2) {
  mat2x3 r;

  r.col0.x = m1.col0.x * m2.col0.x + m1.col1.x * m2.col0.y;
  r.col1.x = m1.col0.x * m2.col1.x + m1.col1.x * m2.col1.y;

  r.col0.y = m1.col0.y * m2.col0.x + m1.col1.y * m2.col0.y;
  r.col1.y = m1.col0.y * m2.col1.x + m1.col1.y * m2.col1.y;

  r.delta.x = m1.delta.x * m2.col0.x + m1.delta.y * m2.col0.y + m2.delta.x;
  r.delta.y = m1.delta.x * m2.col1.x + m1.delta.y * m2.col1.y + m2.delta.y;
  return r;
}

texture::~texture() {}

vertex_buffer::~vertex_buffer() {}

class vertex_buffer_impl final : public vertex_buffer {
public:
  vertex_buffer_impl(const tr2 *tri, std::size_t n) : triangles(n) {
    assert(tri != nullptr);
    for (size_t i = 0; i < n; ++i) {
      triangles[i] = tri[i];
    }
  }
  ~vertex_buffer_impl() final;

  const v2 *data() const final { return &triangles.data()->v[0]; }
  virtual size_t size() const final { return triangles.size() * 3; }

private:
  std::vector<tr2> triangles;
};

vertex_buffer_impl::~vertex_buffer_impl() {}

class texture_gl_es_32 final : public texture {
public:
  explicit texture_gl_es_32(std::string_view path);
  ~texture_gl_es_32() override;

  void bind() const {
    glBindTexture(GL_TEXTURE_2D, tex_handl);
    OM_GL_CHECK()
  }
  std::uint32_t get_width() const final { return width; }
  std::uint32_t get_height() const final { return height; }

private:
  std::string file_path;
  GLuint tex_handl = 0;
  std::uint32_t width = 0;
  std::uint32_t height = 0;
};

class shader_gl_es32 {
public:
  shader_gl_es32(
      std::string_view vertex_src, std::string_view fragment_src,
      const std::vector<std::tuple<GLuint, const GLchar *>> &attributes) {
    vertex_shader = compile_shader(GL_VERTEX_SHADER, vertex_src);
    fragment_shader = compile_shader(GL_FRAGMENT_SHADER, fragment_src);
    if (vertex_shader == 0 || fragment_shader == 0) {
      throw std::runtime_error("can't compile shader");
    }
    program_id = link_shader_program(attributes);
    if (program_id == 0) {
      throw std::runtime_error("can't link shader");
    }
  }

  void use() const {
    glUseProgram(program_id);
    OM_GL_CHECK()
  }

  void set_uniform(std::string_view uniform_name, texture_gl_es_32 *texture) {
    assert(texture != nullptr);
    const int location = glGetUniformLocation(program_id, uniform_name.data());
    OM_GL_CHECK()
    if (location == -1) {
      std::cerr << "can't get uniform location from shader\n";
      throw std::runtime_error("can't get uniform location");
    }
    unsigned int texture_unit = 0;
    glActiveTexture(GL_TEXTURE0 + texture_unit);
    OM_GL_CHECK()

    texture->bind();

    // http://www.khronos.org/opengles/sdk/docs/man/xhtml/glUniform.xml
    glUniform1i(location, static_cast<int>(0 + texture_unit));
    OM_GL_CHECK()
  }

  void set_uniform(std::string_view uniform_name, const color &c) {
    const int location = glGetUniformLocation(program_id, uniform_name.data());
    OM_GL_CHECK()
    if (location == -1) {
      std::cerr << "can't get uniform location from shader\n";
      throw std::runtime_error("can't get uniform location");
    }
    float values[4] = {c.get_r(), c.get_g(), c.get_b(), c.get_a()};
    glUniform4fv(location, 1, &values[0]);
    OM_GL_CHECK()
  }

  void set_uniform(std::string_view uniform_name, const mat2x3 &m) {
    const int location = glGetUniformLocation(program_id, uniform_name.data());
    OM_GL_CHECK();
    if (location == -1) {
      std::cerr << "can't get uniform location from shader\n";
      throw std::runtime_error("can't get uniform location");
    }
    // OpenGL wants matrix in column major order
    // clang-format off
        float values[9] = { m.col0.x,  m.col0.y, m.delta.x,
                            m.col1.x, m.col1.y, m.delta.y,
                            0.f,      0.f,       1.f };
    // clang-format on
    glUniformMatrix3fv(location, 1, GL_FALSE, &values[0]);
    OM_GL_CHECK();
  }

private:
  GLuint compile_shader(GLenum shader_type, std::string_view src) {
    GLuint shader_id = glCreateShader(shader_type);
    OM_GL_CHECK()
    std::string_view vertex_shader_src = src;
    const char *source = vertex_shader_src.data();
    glShaderSource(shader_id, 1, &source, nullptr);
    OM_GL_CHECK()

    glCompileShader(shader_id);
    OM_GL_CHECK()

    GLint compiled_status = 0;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);
    OM_GL_CHECK()
    if (compiled_status == 0) {
      GLint info_len = 0;
      glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
      OM_GL_CHECK()
      std::vector<char> info_chars(static_cast<size_t>(info_len));
      glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data());
      OM_GL_CHECK()
      glDeleteShader(shader_id);
      OM_GL_CHECK()

      std::string shader_type_name =
          shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment";
      std::cerr << "Error compiling shader(vertex)\n"
                << vertex_shader_src << "\n"
                << info_chars.data();
      return 0;
    }
    return shader_id;
  }

  GLuint link_shader_program(
      const std::vector<std::tuple<GLuint, const GLchar *>> &attributes) {
    GLuint program_id_ = glCreateProgram();
    OM_GL_CHECK()
    if (0 == program_id_) {
      std::cerr << "failed to create gl program";
      throw std::runtime_error("can't link shader");
    }

    glAttachShader(program_id_, vertex_shader);
    OM_GL_CHECK()
    glAttachShader(program_id_, fragment_shader);
    OM_GL_CHECK()

    // bind attribute location
    for (const auto &attr : attributes) {
      GLuint loc = std::get<0>(attr);
      const GLchar *name = std::get<1>(attr);
      glBindAttribLocation(program_id_, loc, name);
      OM_GL_CHECK()
    }

    // link program after binding attribute locations
    glLinkProgram(program_id_);
    OM_GL_CHECK()
    // Check the link status
    GLint linked_status = 0;
    glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
    OM_GL_CHECK()
    if (linked_status == 0) {
      GLint infoLen = 0;
      glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
      OM_GL_CHECK()
      std::vector<char> infoLog(static_cast<size_t>(infoLen));
      glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
      OM_GL_CHECK()
      std::cerr << "Error linking program:\n" << infoLog.data();
      glDeleteProgram(program_id_);
      OM_GL_CHECK()
      return 0;
    }
    return program_id_;
  }

  GLuint vertex_shader = 0;
  GLuint fragment_shader = 0;
  GLuint program_id = 0;
};

static std::array<std::string_view, 15> event_names{
    {"UP_pressed", "UP_released", "LEFT_pressed", "LEFT_released",
     "DOWN_pressed", "DOWN_released", "RIGHT_pressed", "RIGHT_released",
     "FIRE_pressed", "FIRE_released", "PAUSE_pressed", "PAUSE_released",
     "PLAY_pressed", "PLAY_released", "TURN_off"}};

std::ostream &operator<<(std::ostream &stream, const event e) {
  std::uint32_t value = static_cast<std::uint32_t>(e);
  std::uint32_t minimal = static_cast<std::uint32_t>(event::up_pressed);
  std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
  if (value >= minimal && value <= maximal) {
    stream << event_names[value];
    return stream;
  } else {
    throw std::runtime_error("Too big event value");
  }
}

tr0::tr0() : v{v0(), v0(), v0()} {}

tr1::tr1() : v{v1(), v1(), v1()} {}

tr2::tr2() : v{v2(), v2(), v2()} {}

std::ostream &operator<<(std::ostream &out, const SDL_version &v) {
  out << static_cast<int>(v.major) << '.';
  out << static_cast<int>(v.minor) << '.';
  out << static_cast<int>(v.patch);
  return out;
}

std::istream &operator>>(std::istream &is, mat2x3 &m) {
  is >> m.col0.x;
  is >> m.col1.x;
  is >> m.col0.y;
  is >> m.col1.y;
  return is;
}

std::istream &operator>>(std::istream &is, vec2 &v) {
  is >> v.x;
  is >> v.y;
  return is;
}

std::istream &operator>>(std::istream &is, color &c) {
  float r = 0.f;
  float g = 0.f;
  float b = 0.f;
  float a = 0.f;
  is >> r;
  is >> g;
  is >> b;
  is >> a;
  c = color(r, g, b, a);
  return is;
}

std::istream &operator>>(std::istream &is, v0 &v) {
  is >> v.pos.x;
  is >> v.pos.y;

  return is;
}

std::istream &operator>>(std::istream &is, v1 &v) {
  is >> v.pos.x;
  is >> v.pos.y;
  is >> v.c;
  return is;
}

std::istream &operator>>(std::istream &is, v2 &v) {
  is >> v.pos.x;
  is >> v.pos.y;
  is >> v.uv;
  is >> v.c;
  return is;
}

std::istream &operator>>(std::istream &is, tr0 &t) {
  is >> t.v[0];
  is >> t.v[1];
  is >> t.v[2];
  return is;
}

std::istream &operator>>(std::istream &is, tr1 &t) {
  is >> t.v[0];
  is >> t.v[1];
  is >> t.v[2];
  return is;
}

std::istream &operator>>(std::istream &is, tr2 &t) {
  is >> t.v[0];
  is >> t.v[1];
  is >> t.v[2];
  return is;
}

struct bind_keyboard {
  SDL_Keycode key;
  std::string_view name;
  event event_pressed;
  event event_released;

  keys engine_key;
};

const std::array<bind_keyboard, 8> keys{
    {{SDLK_w, "UP", event::up_pressed, event::up_released, keys::up},
     {SDLK_a, "LEFT", event::left_pressed, event::left_released, keys::left},
     {SDLK_s, "DOWN", event::dowm_pressed, event::down_released, keys::down},
     {SDLK_d, "RIGHT", event::right_pressed, event::right_released,
      keys::right},
     {SDLK_SPACE, "FIRE", event::fire_pressed, event::fire_released,
      keys::fire},
     {SDLK_ESCAPE, "PAUSE", event::pause_pressed, event::pause_released,
      keys::pause},
     {SDLK_KP_ENTER, "PLAY", event::play_pressed, event::play_released,
      keys::start}}};
static bool check_input_keyboard_event(const SDL_Event &e,
                                       const bind_keyboard *&result) {
  using namespace std;
  const auto it_for_keyboard =
      find_if(begin(keys), end(keys), [&](const bind_keyboard &b) {
        return b.key == e.key.keysym.sym;
      });
  if (it_for_keyboard != end(keys)) {
    result = &(*it_for_keyboard);
    return true;
  }
  return false;
}

struct bind_controller {
  SDL_GameControllerButton button;
  std::string_view name;
  event event_pressed;
  event event_released;
};

const std::array<bind_controller, 8> controller_buttons{
    {{SDL_CONTROLLER_BUTTON_DPAD_UP, "UP", event::up_pressed,
      event::up_released},
     {SDL_CONTROLLER_BUTTON_DPAD_LEFT, "LEFT", event::left_pressed,
      event::left_released},
     {SDL_CONTROLLER_BUTTON_DPAD_DOWN, "DOWN", event::dowm_pressed,
      event::down_released},
     {SDL_CONTROLLER_BUTTON_DPAD_RIGHT, "RIGHT", event::right_pressed,
      event::right_released},
     {SDL_CONTROLLER_BUTTON_X, "FIRE", event::fire_pressed,
      event::fire_released},
     {SDL_CONTROLLER_BUTTON_A, "PAUSE", event::pause_pressed,
      event::pause_released},
     {SDL_CONTROLLER_BUTTON_B, "PLAY", event::play_pressed,
      event::play_released}}};

static bool check_input_controller_event(const SDL_Event &e,
                                         const bind_controller *&result) {
  using namespace std;
  const auto it_for_controller = find_if(
      begin(controller_buttons), end(controller_buttons),
      [&](const bind_controller &b) { return b.button == e.cbutton.state; });
  if (it_for_controller != end(controller_buttons)) {
    result = &(*it_for_controller);
    return true;
  }
  return false;
}

class engine_impl final : public engine {
public:
  std::string initialize(std::string_view) final;

  float get_time_from_init() final {
    std::uint32_t ms_from_library_initialization = SDL_GetTicks();
    float seconds = ms_from_library_initialization * 0.001f;
    return seconds;
  }

  texture *create_texture(std::string_view path) final {
    return new texture_gl_es_32(path);
  }

  bool read_inrut(event &e) final {
    using namespace std;
    SDL_Event sdl_event;
    if (SDL_PollEvent(&sdl_event)) {
      const bind_keyboard *binding_keyboard = nullptr;
      const bind_controller *binding_controller = nullptr;
      if (sdl_event.type == SDL_QUIT) {
        e = event::turn_off;
        return true;
      } else if (sdl_event.type == SDL_KEYDOWN) {
        if (check_input_keyboard_event(sdl_event, binding_keyboard)) {
          e = binding_keyboard->event_pressed;
          return true;
        }
      } else if (sdl_event.type == SDL_KEYUP) {
        if (check_input_keyboard_event(sdl_event, binding_keyboard)) {
          e = binding_keyboard->event_released;
          return true;
        }
      } else if (sdl_event.type == SDL_CONTROLLERDEVICEADDED) {
        cerr << "Controller added" << endl;
      } else if (sdl_event.type == SDL_CONTROLLERDEVICEREMOVED) {
        cerr << "Controller removed" << endl;
      } else if (sdl_event.type == SDL_CONTROLLERBUTTONDOWN) {
        if (check_input_controller_event(sdl_event, binding_controller)) {
          e = binding_controller->event_pressed;
          return true;
        }
      } else if (sdl_event.type == SDL_CONTROLLERBUTTONUP) {
        if (check_input_controller_event(sdl_event, binding_controller)) {
          e = binding_controller->event_released;
          return true;
        }
      }
    }
    return false;
  }

  bool is_key_down(const enum keys key) final {
    const auto it =
        std::find_if(begin(keys), end(keys), [&](const bind_keyboard &b) {
          return b.engine_key == key;
        });

    if (it != end(keys)) {
      const std::uint8_t *state = SDL_GetKeyboardState(nullptr);
      int sdl_scan_code = SDL_GetScancodeFromKey(it->key);
      return state[sdl_scan_code];
    }
    return false;
  }

  void destroy_texture(texture *t) final { delete t; }

  vertex_buffer *create_vertex_buffer(const tr2 *triangles, std::size_t n) {
    return new vertex_buffer_impl(triangles, n);
  }
  void destroy_vertex_buffer(vertex_buffer *buffer) { delete buffer; }

  void render(const tr0 &t, const color &c) final {
    shader00->use();
    shader00->set_uniform("u_color", c);
    // vertex coordinates
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(v0), &t.v[0].pos.x);
    OM_GL_CHECK()
    glEnableVertexAttribArray(0);
    OM_GL_CHECK()

    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK()
  }
  void render(const tr1 &t) final {
    shader01->use();
    // positions
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(t.v[0]),
                          &t.v[0].pos);
    OM_GL_CHECK()
    glEnableVertexAttribArray(0);
    OM_GL_CHECK()
    // colors
    glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(t.v[0]),
                          &t.v[0].c);
    OM_GL_CHECK()
    glEnableVertexAttribArray(1);
    OM_GL_CHECK()

    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK()

    glDisableVertexAttribArray(1);
    OM_GL_CHECK()
  }

  void render(const tr2 &t, texture *tex) final {
    shader02->use();
    texture_gl_es_32 *texture = static_cast<texture_gl_es_32 *>(tex);
    texture->bind();
    shader02->set_uniform("s_texture", texture);
    // positions
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(t.v[0]),
                          &t.v[0].pos);
    OM_GL_CHECK()
    glEnableVertexAttribArray(0);
    OM_GL_CHECK()
    // colors
    glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(t.v[0]),
                          &t.v[0].c);
    OM_GL_CHECK()
    glEnableVertexAttribArray(1);
    OM_GL_CHECK()

    // texture coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(t.v[0]), &t.v[0].uv);
    OM_GL_CHECK()
    glEnableVertexAttribArray(2);
    OM_GL_CHECK()

    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK()

    glDisableVertexAttribArray(1);
    OM_GL_CHECK()
    glDisableVertexAttribArray(2);
    OM_GL_CHECK()
  }

  void render(const tr2 &t, texture *tex, const mat2x3 &m) {
    shader03->use();
    texture_gl_es_32 *texture = static_cast<texture_gl_es_32 *>(tex);
    texture->bind();
    shader03->set_uniform("s_texture", texture);
    shader03->set_uniform("u_matrix", m);
    // positions
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(t.v[0]),
                          &t.v[0].pos);
    OM_GL_CHECK();
    glEnableVertexAttribArray(0);
    OM_GL_CHECK();
    // colors
    glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(t.v[0]),
                          &t.v[0].c);
    OM_GL_CHECK();
    glEnableVertexAttribArray(1);
    OM_GL_CHECK();

    // texture coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(t.v[0]), &t.v[0].uv);
    OM_GL_CHECK();
    glEnableVertexAttribArray(2);
    OM_GL_CHECK();

    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK();

    glDisableVertexAttribArray(1);
    OM_GL_CHECK();
    glDisableVertexAttribArray(2);
    OM_GL_CHECK();
  }

  void render(const vertex_buffer &buff, texture *tex, const mat2x3 &m) final {
    shader03->use();
    texture_gl_es_32 *texture = static_cast<texture_gl_es_32 *>(tex);
    texture->bind();
    shader03->set_uniform("s_texture", texture);
    shader03->set_uniform("u_matrix", m);

    assert(gl_default_vbo != 0);

    glBindBuffer(GL_ARRAY_BUFFER, gl_default_vbo);
    OM_GL_CHECK();

    const v2 *t = buff.data();
    uint32_t data_size_in_bytes =
        static_cast<uint32_t>(buff.size() * sizeof(v2));
    glBufferData(GL_ARRAY_BUFFER, data_size_in_bytes, t, GL_DYNAMIC_DRAW);
    OM_GL_CHECK();
    glBufferSubData(GL_ARRAY_BUFFER, 0, data_size_in_bytes, t);
    OM_GL_CHECK();

    // positions
    glEnableVertexAttribArray(0);
    OM_GL_CHECK();
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(v2), nullptr);
    OM_GL_CHECK();
    // colors
    glVertexAttribPointer(
        1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(v2),
        reinterpret_cast<void *>(sizeof(v2::pos) + sizeof(v2::uv)));
    OM_GL_CHECK();
    glEnableVertexAttribArray(1);
    OM_GL_CHECK();

    // texture coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(v2),
                          reinterpret_cast<void *>(sizeof(v2::pos)));
    OM_GL_CHECK();
    glEnableVertexAttribArray(2);
    OM_GL_CHECK();

    GLsizei num_of_vertexes = static_cast<GLsizei>(buff.size());
    glDrawArrays(GL_TRIANGLES, 0, num_of_vertexes);
    OM_GL_CHECK();

    glDisableVertexAttribArray(1);
    OM_GL_CHECK();
    glDisableVertexAttribArray(2);
    OM_GL_CHECK();
  }

  void swap_buffers() final {
    SDL_GL_SwapWindow(window);

    glClear(GL_COLOR_BUFFER_BIT);
    OM_GL_CHECK()
  }
  void uninitialize() final {
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
  }

private:
  SDL_Window *window = nullptr;
  SDL_GLContext gl_context = nullptr;

  shader_gl_es32 *shader00 = nullptr;
  shader_gl_es32 *shader01 = nullptr;
  shader_gl_es32 *shader02 = nullptr;
  shader_gl_es32 *shader03 = nullptr;
  uint32_t gl_default_vbo = 0;
};

static bool already_exist = false;
engine *create_engine() {
  if (already_exist) {
    throw std::runtime_error("engine already exist");
  }
  engine *result = new engine_impl();
  already_exist = true;
  return result;
}

void destroy_engine(engine *e) {
  if (already_exist == false) {
    throw std::runtime_error("engine not created");
  }
  if (nullptr == e) {
    throw std::runtime_error("e is nullptr");
  }
  delete e;
}

color::color(std::uint32_t rgba_) : rgba(rgba_) {}

color::color(float r, float g, float b, float a) {
  assert(r <= 1 && r >= 0);
  assert(g <= 1 && g >= 0);
  assert(b <= 1 && b >= 0);
  assert(a <= 1 && a >= 0);

  std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
  std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
  std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
  std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);

  rgba = a_ << 24 | b_ << 16 | g_ << 8 | r_;
}

float color::get_r() const {
  std::uint32_t r_ = (rgba & 0x000000FF) >> 0;
  return r_ / 255.f;
}
float color::get_g() const {
  std::uint32_t g_ = (rgba & 0x0000FF00) >> 8;
  return g_ / 255.f;
}
float color::get_b() const {
  std::uint32_t b_ = (rgba & 0x00FF0000) >> 16;
  return b_ / 255.f;
}
float color::get_a() const {
  std::uint32_t a_ = (rgba & 0xFF000000) >> 24;
  return a_ / 255.f;
}

void color::set_r(const float r) {
  std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
  rgba &= 0xFFFFFF00;
  rgba |= (r_ << 0);
}
void color::set_g(const float g) {
  std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
  rgba &= 0xFFFF00FF;
  rgba |= (g_ << 8);
}
void color::set_b(const float b) {
  std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
  rgba &= 0xFF00FFFF;
  rgba |= (b_ << 16);
}
void color::set_a(const float a) {
  std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);
  rgba &= 0x00FFFFFF;
  rgba |= a_ << 24;
}

engine::~engine() {}

texture_gl_es_32::texture_gl_es_32(std::string_view path) : file_path(path) {
  std::vector<unsigned char> png_file_in_memory;
  std::ifstream ifs(path.data(), std::ios_base::binary);
  if (!ifs) {
    throw std::runtime_error("can't load texture");
  }
  ifs.seekg(0, std::ios_base::end);
  std::streamoff pos_in_file = ifs.tellg();
  png_file_in_memory.resize(static_cast<size_t>(pos_in_file));
  ifs.seekg(0, std::ios_base::beg);
  if (!ifs) {
    throw std::runtime_error("can't load texture");
  }

  ifs.read(reinterpret_cast<char *>(png_file_in_memory.data()), pos_in_file);
  if (!ifs.good()) {
    throw std::runtime_error("can't load texture");
  }

  std::vector<unsigned char> image;
  unsigned long w = 0;
  unsigned long h = 0;
  int error = decodePNG(image, w, h, &png_file_in_memory[0],
                        png_file_in_memory.size(), false);

  // if there's an error, display it
  if (error != 0) {
    std::cerr << "error: " << error << std::endl;
    throw std::runtime_error("can't load texture");
  }

  glGenTextures(1, &tex_handl);
  OM_GL_CHECK()
  glBindTexture(GL_TEXTURE_2D, tex_handl);
  OM_GL_CHECK()

  GLint mipmap_level = 0;
  GLint border = 0;
  GLsizei width_ = static_cast<GLsizei>(w);
  GLsizei height_ = static_cast<GLsizei>(h);
  glTexImage2D(GL_TEXTURE_2D, mipmap_level, GL_RGBA, width_, height_, border,
               GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);
  OM_GL_CHECK()

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  OM_GL_CHECK()
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  OM_GL_CHECK()
}

texture_gl_es_32::~texture_gl_es_32() {
  glDeleteTextures(1, &tex_handl);
  OM_GL_CHECK()
}

std::string engine_impl::initialize(std::string_view) {
  using namespace std;

  stringstream serr;

  const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (init_result != 0) {
    const char *err_message = SDL_GetError();
    serr << "error: failed call SDL_Init: " << err_message << endl;
    return serr.str();
  }

  window =
      SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       800, 600, ::SDL_WINDOW_OPENGL);

  if (window == nullptr) {
    const char *err_message = SDL_GetError();
    serr << "error: failed call SDL_CreateWindow: " << err_message << endl;
    SDL_Quit();
    return serr.str();
  }

  int gl_major_version = 3;
  int gl_minor_version = 2;
  int gl_context_profile = SDL_GL_CONTEXT_PROFILE_ES;

  std::string_view platform = SDL_GetPlatform();
  auto list = {"Windows", "Mac OS X"};
  auto it = find(begin(list), end(list), platform);
  if (it != end(list)) {
    gl_major_version = 4;
    gl_minor_version = 6;
    gl_context_profile = SDL_GL_CONTEXT_PROFILE_CORE;
  } else {
    gl_major_version = 3;
    gl_minor_version = 2;
    gl_context_profile = SDL_GL_CONTEXT_PROFILE_ES;
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, gl_context_profile);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_version);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_version);

  gl_context = SDL_GL_CreateContext(window);
  if (gl_context == nullptr) {
    std::string msg("can't create opengl context: ");
    msg += SDL_GetError();
    serr << msg << endl;
    return serr.str();
  }

  gl_major_version = 0;
  gl_minor_version = 0;

  int result =
      SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_version);
  assert(result == 0);

  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_version);
  assert(result == 0);

  if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) {
    std::clog << "error: failed to initialize glad" << std::endl;
  }

  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  glDebugMessageCallback(callback_opengl_debug, nullptr);
  glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr,
                        GL_TRUE);

  glGenBuffers(1, &gl_default_vbo);
  OM_GL_CHECK();
  glBindBuffer(GL_ARRAY_BUFFER, gl_default_vbo);
  OM_GL_CHECK();
  uint32_t data_size_in_bytes = 0;
  glBufferData(GL_ARRAY_BUFFER, data_size_in_bytes, nullptr, GL_STATIC_DRAW);
  OM_GL_CHECK();
  glBufferSubData(GL_ARRAY_BUFFER, 0, data_size_in_bytes, nullptr);
  OM_GL_CHECK();

  shader00 = new shader_gl_es32(
      R"(
      attribute vec2 a_position;
      void main()
      {
        gl_Position = vec4(a_position, 0.0, 1.0);
      }
      )",
      R"(
      precision lowp float;
      uniform vec4 u_color;
      void main()
      {
        gl_FragColor = u_color;
      }
      )",
      {{0, "a_position"}});

  shader00->use();
  shader00->set_uniform("u_color", color(1.f, 0.f, 0.f, 1.f));

  shader01 = new shader_gl_es32(
      R"(
      attribute vec2 a_position;
      attribute vec4 a_color;
      varying vec4 v_color;
      void main()
      {
        v_color = a_color;
        gl_Position = vec4(a_position, 0.0, 1.0);
      }
      )",
      R"(
      precision lowp float;
      varying vec4 v_color;
      void main()
      {
        gl_FragColor = v_color;
      }
      )",
      {{0, "a_position"}, {1, "a_color"}});

  shader01->use();

  shader02 = new shader_gl_es32(
      R"(
      attribute vec2 a_position;
      attribute vec2 a_tex_coord;
      attribute vec4 a_color;
      varying vec4 v_color;
      varying vec2 v_tex_coord;
      void main()
      {
        v_tex_coord = a_tex_coord;
        v_color = a_color;
        gl_Position = vec4(a_position, 0.0, 1.0);
      }
      )",
      R"(
      precision mediump float;
      varying vec2 v_tex_coord;
      varying vec4 v_color;
      uniform sampler2D s_texture;
      void main()
      {
        gl_FragColor = texture2D(s_texture, v_tex_coord) * v_color;
      }
      )",
      {{0, "a_position"}, {1, "a_color"}, {2, "a_tex_coord"}});

  // turn on rendering with just created shader program
  shader02->use();

  shader03 = new shader_gl_es32(
      R"(
      uniform mat3 u_matrix;
      attribute vec2 a_position;
      attribute vec2 a_tex_coord;
      attribute vec4 a_color;
      varying vec4 v_color;
      varying vec2 v_tex_coord;
      void main()
      {
        v_tex_coord = a_tex_coord;
        v_color = a_color;
        vec3 pos = vec3(a_position, 1.0) * u_matrix;
        gl_Position = vec4(pos, 1.0);
      }
      )",
      R"(
      precision mediump float;
      varying vec2 v_tex_coord;
      varying vec4 v_color;
      uniform sampler2D s_texture;
      void main()
      {
        gl_FragColor = texture2D(s_texture, v_tex_coord) * v_color;
      }
      )",
      {{0, "a_position"}, {1, "a_color"}, {2, "a_tex_coord"}});
  shader03->use();

  glEnable(GL_BLEND);
  OM_GL_CHECK()
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  OM_GL_CHECK()

  glClearColor(0.f, 0.0, 0.f, 0.0f);
  OM_GL_CHECK()

  return "";
}

static const char *source_to_strv(GLenum source) {
  switch (source) {
  case GL_DEBUG_SOURCE_API:
    return "API";
  case GL_DEBUG_SOURCE_SHADER_COMPILER:
    return "SHADER_COMPILER";
  case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
    return "WINDOW_SYSTEM";
  case GL_DEBUG_SOURCE_THIRD_PARTY:
    return "THIRD_PARTY";
  case GL_DEBUG_SOURCE_APPLICATION:
    return "APPLICATION";
  case GL_DEBUG_SOURCE_OTHER:
    return "OTHER";
  }
  return "unknown";
}

static const char *type_to_strv(GLenum type) {
  switch (type) {
  case GL_DEBUG_TYPE_ERROR:
    return "ERROR";
  case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
    return "DEPRECATED_BEHAVIOR";
  case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
    return "UNDEFINED_BEHAVIOR";
  case GL_DEBUG_TYPE_PERFORMANCE:
    return "PERFORMANCE";
  case GL_DEBUG_TYPE_PORTABILITY:
    return "PORTABILITY";
  case GL_DEBUG_TYPE_MARKER:
    return "MARKER";
  case GL_DEBUG_TYPE_PUSH_GROUP:
    return "PUSH_GROUP";
  case GL_DEBUG_TYPE_POP_GROUP:
    return "POP_GROUP";
  case GL_DEBUG_TYPE_OTHER:
    return "OTHER";
  }
  return "unknown";
}

static const char *severity_to_strv(GLenum severity) {
  switch (severity) {
  case GL_DEBUG_SEVERITY_HIGH:
    return "HIGH";
  case GL_DEBUG_SEVERITY_MEDIUM:
    return "MEDIUM";
  case GL_DEBUG_SEVERITY_LOW:
    return "LOW";
  case GL_DEBUG_SEVERITY_NOTIFICATION:
    return "NOTIFICATION";
  }
  return "unknown";
}

// 30Kb on my system, too much for stack
static std::array<char, GL_MAX_DEBUG_MESSAGE_LENGTH> local_log_buff;

static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar *message, [[maybe_unused]] const void *userParam) {
  // The memory formessageis owned and managed by the GL, and should onlybe
  // considered valid for the duration of the function call.The behavior of
  // calling any GL or window system function from within thecallback function
  // is undefined and may lead to program termination.Care must also be taken
  // in securing debug callbacks for use with asynchronousdebug output by
  // multi-threaded GL implementations.  Section 18.8 describes thisin further
  // detail.

  auto &buff{local_log_buff};
  int num_chars = std::snprintf(buff.data(), buff.size(), "%s %s %d %s %.*s\n",
                                source_to_strv(source), type_to_strv(type), id,
                                severity_to_strv(severity), length, message);

  if (num_chars > 0) {
    // TODO use https://en.cppreference.com/w/cpp/io/basic_osyncstream
    // to fix possible data races
    // now we use GL_DEBUG_OUTPUT_SYNCHRONOUS to garantie call in main
    // thread
    std::cerr.write(buff.data(), num_chars);
  }
}

}; // namespace engine
