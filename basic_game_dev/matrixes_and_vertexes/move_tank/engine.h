#include <iosfwd>
#include <string>
#include <string_view>

#ifndef ENGINE_DECLSPEC
#define ENGINE_DECLSPEC
#endif

namespace engine {

struct ENGINE_DECLSPEC vec2 {
  vec2();
  vec2(float x, float y);
  float x;
  float y;
};

ENGINE_DECLSPEC vec2 &operator+(const vec2 &left, const vec2 &right);

struct ENGINE_DECLSPEC mat2x3 {
  mat2x3();
  static mat2x3 identiry();
  static mat2x3 scale(float scale);
  static mat2x3 scale(float sx, float sy);
  static mat2x3 rotation(float thetha);
  static mat2x3 move(const vec2 &delta);

  vec2 col0;
  vec2 col1;
  vec2 delta;
};

ENGINE_DECLSPEC vec2 operator*(const vec2 &vector, const mat2x3 &matrix);
ENGINE_DECLSPEC mat2x3 operator*(const mat2x3 &matrix0, const mat2x3 &matrix1);

enum class event {
  up_pressed,
  up_released,
  left_pressed,
  left_released,
  dowm_pressed,
  down_released,
  right_pressed,
  right_released,
  fire_pressed,
  fire_released,
  pause_pressed,
  pause_released,
  play_pressed,
  play_released,
  turn_off
};

ENGINE_DECLSPEC std::ostream &operator<<(std::ostream &stream, const event e);

enum class keys { left, right, up, down, start, fire, pause };

class engine;

ENGINE_DECLSPEC engine *create_engine();
ENGINE_DECLSPEC void destroy_engine(engine *eng);

class color {
public:
  color() = default;
  explicit color(std::uint32_t rgba_);
  color(float r, float g, float b, float a);

  float get_r() const;
  float get_g() const;
  float get_b() const;
  float get_a() const;

  void set_r(const float r);
  void set_g(const float g);
  void set_b(const float b);
  void set_a(const float a);

private:
  std::uint32_t rgba = 0;
};

// vertex with pos only
struct v0 {
  vec2 pos;
};

// vertex with pos and color
struct v1 {
  vec2 pos;
  color c;
};

// vertex with pos and texture pos and color
struct v2 {
  vec2 pos;
  vec2 uv;
  color c;
};

// triangle with pos only
struct tr0 {
  tr0();
  v0 v[3];
};

// triangle with pos and color
struct tr1 {
  tr1();
  v1 v[3];
};

// triangle with pos, color and texture pos
struct tr2 {
  tr2();
  v2 v[3];
};

std::istream &operator>>(std::istream &is, mat2x3 &);
std::istream &operator>>(std::istream &is, vec2 &);
std::istream &operator>>(std::istream &stream, color &);
std::istream &operator>>(std::istream &stream, v0 &);
std::istream &operator>>(std::istream &stream, v1 &);
std::istream &operator>>(std::istream &stream, v2 &);
std::istream &operator>>(std::istream &stream, tr0 &);
std::istream &operator>>(std::istream &stream, tr1 &);
std::istream &operator>>(std::istream &stream, tr2 &);

class texture {
public:
  virtual ~texture();
  virtual std::uint32_t get_width() const = 0;
  virtual std::uint32_t get_height() const = 0;
};

class vertex_buffer {
public:
  virtual ~vertex_buffer();
  virtual const v2 *data() const = 0;
  /// count of vertexes
  virtual size_t size() const = 0;
};

class ENGINE_DECLSPEC engine {
public:
  virtual ~engine();
  // create main window
  virtual std::string initialize(std::string_view config) = 0;
  // return count of seconds after initialize
  virtual float get_time_from_init() = 0;
  virtual bool is_key_down(const keys) = 0;
  virtual bool read_inrut(event &e) = 0;
  virtual texture *create_texture(std::string_view path) = 0;
  virtual void destroy_texture(texture *t) = 0;

  virtual vertex_buffer *create_vertex_buffer(const tr2 *, std::size_t) = 0;
  virtual void destroy_vertex_buffer(vertex_buffer *) = 0;

  virtual void render(const tr0 &, const color &) = 0;
  virtual void render(const tr1 &) = 0;
  virtual void render(const tr2 &, texture *) = 0;
  virtual void render(const tr2 &, texture *, const mat2x3 &) = 0;
  virtual void render(const vertex_buffer &, texture *, const mat2x3 &) = 0;
  virtual void swap_buffers() = 0;
  virtual void uninitialize() = 0;
};

} // namespace engine
