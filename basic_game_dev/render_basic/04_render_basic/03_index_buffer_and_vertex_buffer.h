#pragma once

#include "02_draw_triangle.h"

struct triangle_index_render : triangle_render {
  triangle_index_render(image &buffer, size_t width, size_t height);
  void draw_triangle(std::vector<position> &vertexes,
                     std::vector<uint8_t> &indexes, color c);
};
