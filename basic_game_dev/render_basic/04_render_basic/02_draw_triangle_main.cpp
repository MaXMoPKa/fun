#include "02_draw_triangle.h"

int main(int /*argc*/, char ** /*argv*/, char ** /*env*/) {

  color black{0, 0, 0};
  color green{0, 255, 0};
  color blue{0, 0, 255};
  color red{255, 0, 0};

  image first_triangle;

  triangle_render trian_render(first_triangle, width, height);
  trian_render.clear(black);

  std::vector<position> vertexes;
  vertexes.push_back(position{0, 0});
  vertexes.push_back(position{width - 1, height - 1});
  vertexes.push_back(position{0, height - 1});

  trian_render.draw_triangle(vertexes, 3, green);

  first_triangle.save_image("First triangles");

  trian_render.clear(black);

  size_t max_x = 10;
  size_t max_y = 10;
  std::vector<position> triangles;
  for (size_t i = 0; i < max_x; ++i) {
    for (size_t j = 0; j < max_y; j++) {
      int32_t step_x = (width - 1) / max_x;
      int32_t step_y = (height - 1) / max_y;

      position ver0{5 + static_cast<int>(i) * step_x,
                    5 + static_cast<int>(j) * step_y};
      position ver1{ver0.x + step_x, ver0.y + step_y};
      position ver2{ver0.x, ver0.y + step_y};
      position ver3{ver0.x + step_x, ver0.y};

      triangles.push_back(ver0);
      triangles.push_back(ver2);
      triangles.push_back(ver2);

      triangles.push_back(ver0);
      triangles.push_back(ver3);
      triangles.push_back(ver3);

      triangles.push_back(ver3);
      triangles.push_back(ver1);
      triangles.push_back(ver1);

      triangles.push_back(ver1);
      triangles.push_back(ver2);
      triangles.push_back(ver2);
    }
  }
  trian_render.draw_triangle(triangles, triangles.size(), red);
  first_triangle.save_image("There are a lot of triangles!!!");
  return EXIT_SUCCESS;
}
