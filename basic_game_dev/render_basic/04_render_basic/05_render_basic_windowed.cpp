#include "04_triangle_interpolated_render.h"

#include <SDL.h>

#include <iostream>

int main(int /*argc*/, char ** /*argv*/, char ** /*env*/) {
  using namespace std;

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    cerr << "Unable to initialize SDL: " << SDL_GetError() << endl;
    return EXIT_FAILURE;
  }

  SDL_Window *window;
  window = SDL_CreateWindow("Runtime soft render", SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, width, height,
                            SDL_WINDOW_OPENGL | SDL_WINDOW_MOUSE_FOCUS);
  if (window == nullptr) {
    cerr << "Could not create window: " << SDL_GetError() << endl;
    return EXIT_FAILURE;
  }

  SDL_Renderer *render =
      SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  if (render == nullptr) {
    cerr << "Could not create render: " << SDL_GetError() << endl;
    return EXIT_FAILURE;
  }

  const color black{0, 0, 0};
  image image;
  triangle_interpolated interpolated_render(image, width, height);

  struct program : gfx_program {
    double mouse_x{};
    double mouse_y{};
    double radius{};

    void set_uniforms(const uniforms &a_uniforms) override {
      mouse_x = a_uniforms.f0;
      mouse_y = a_uniforms.f1;
      radius = a_uniforms.f2;
    }
    vertex vertex_shader(const vertex &v_in) override {
      vertex out = v_in;
      return out;
    }
    color fragment_shader(const vertex &v_in) override {
      color out;
      out.r = static_cast<uint8_t>(v_in.f2 * 255);
      out.g = static_cast<uint8_t>(v_in.f3 * 255);
      out.b = static_cast<uint8_t>(v_in.f4 * 255);

      double x = v_in.f0;
      double y = v_in.f1;
      double dx = mouse_x - x;
      double dy = mouse_y - y;

      if (dx * dx + dy * dy < radius * radius) {
        double gray = 0.21 * out.r + 0.72 * out.g + 0.07 * out.b;
        out.r = 0.21 * out.r;
        out.g = 0.72 * out.g;
        out.b = 0.07 * out.b;
      }
      return out;
    }

  } program01;

  std::vector<vertex> triangle_v{{0, 0, 1, 0, 0, 0, 0, 0},
                                 {0, 239, 0, 1, 0, 0, 239, 0},
                                 {319, 239, 0, 0, 1, 319, 239, 0}};
  std::vector<uint32_t> indexes_v{0, 1, 2};
  void *pixels = image.data();
  const int depth = sizeof(color) * 8;
  const int pitch = sizeof(color) * width;
  const int rmask = 0x000000ff;
  const int gmask = 0x0000ff00;
  const int bmask = 0x00ff0000;
  const int amask = 0;

  interpolated_render.set_gfx_program(program01);

  double mouse_x{};
  double mouse_y{};
  double radius{20.0};

  bool continue_loop = true;

  while (continue_loop) {
    SDL_Event e;
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        continue_loop = false;
        break;
      } else if (e.type == SDL_MOUSEMOTION) {
        mouse_x = e.motion.x;
        mouse_y = e.motion.y;
      } else if (e.type == SDL_MOUSEWHEEL) {
        radius *= e.wheel.y;
      }
    }
    interpolated_render.clear(black);
    program01.set_uniforms(uniforms{mouse_x, mouse_y, radius});
    interpolated_render.draw_triangle(triangle_v, indexes_v);

    SDL_Surface *bitmapSurface = SDL_CreateRGBSurfaceFrom(
        pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);

    if (bitmapSurface == nullptr) {
      cerr << "Could not create bitmapSurface: " << SDL_GetError() << endl;
      return EXIT_FAILURE;
    }
    SDL_Texture *bitmapTex =
        SDL_CreateTextureFromSurface(render, bitmapSurface);
    if (bitmapTex == nullptr) {
      cerr << "Could not create bitmapTex: " << SDL_GetError() << endl;
      return EXIT_FAILURE;
    }

    SDL_FreeSurface(bitmapSurface);
    SDL_RenderClear(render);
    SDL_RenderCopy(render, bitmapTex, nullptr, nullptr);
    SDL_RenderPresent(render);

    SDL_DestroyTexture(bitmapTex);
  }

  SDL_DestroyRenderer(render);
  SDL_DestroyWindow(window);

  SDL_Quit();

  return EXIT_SUCCESS;
}
