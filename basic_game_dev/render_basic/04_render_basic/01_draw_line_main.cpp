#include "01_draw_line.h"

int main(int /*argc*/, char ** /*argv*/, char ** /*env*/) {
  color black = {0, 0, 0};
  color green = {0, 255, 0};
  color blue = {0, 0, 255};
  color red = {255, 0, 0};

  image image_for_line;

  line_render lrender(image_for_line, width, height);
  lrender.clear(black);
  lrender.draw_line(position{0, 0}, position{319, 239}, green);
  lrender.draw_line(position{0, 0}, position{319, 0}, red);
  lrender.draw_line(position{0, 0}, position{0, 239}, blue);
  lrender.draw_line(position{0, 239}, position{319, 239}, red);
  lrender.draw_line(position{319, 239}, position{319, 0}, blue);
  lrender.draw_line(position{319, 0}, position{0, 239}, green);
  image_for_line.save_image("01_lines.ppm");
  return EXIT_SUCCESS;
}
