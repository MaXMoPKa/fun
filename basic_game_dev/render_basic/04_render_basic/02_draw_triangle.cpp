#include "02_draw_triangle.h"

triangle_render::triangle_render(image &buffer, size_t width, size_t height)
    : line_render(buffer, width, height) {}

pixels triangle_render::pixels_position(position ver0, position ver1,
                                        position ver2) {
  using namespace std;
  pixels pixels_position;
  for (auto [start, end] :
       {pair{ver0, ver1}, pair{ver1, ver2}, pair{ver2, ver0}}) {
    for (auto pos : line_render::pixels_positions(start, end)) {
      pixels_position.push_back(pos);
    }
  }
  return pixels_position;
}

void triangle_render::draw_triangle(std::vector<position> &vertexes,
                                    size_t num_vertexes, color c) {
  pixels triangles_pixels;
  for (size_t i = 0; i < num_vertexes / 3; ++i) {
    position ver0 = vertexes.at(i * 3 + 0);
    position ver1 = vertexes.at(i * 3 + 1);
    position ver2 = vertexes.at(i * 3 + 2);
    for (auto pixels_position : pixels_position(ver0, ver1, ver2)) {
      triangles_pixels.push_back(pixels_position);
    }
  }
  for (auto pos : triangles_pixels) {
    draw_pixel(pos, c);
  }
}
