#include "03_index_buffer_and_vertex_buffer.h"

int main(int /*argc*/, char ** /*argv*/, char ** /*env*/) {
  const color green{0, 255, 0};
  const color blue{0, 0, 255};
  const color red{255, 0, 0};
  const color black{0, 0, 0};

  image ib_triangles;

  triangle_index_render trian_i_render(ib_triangles, width, height);
  trian_i_render.clear(black);

  std::vector<position> vertexes_buffer;
  size_t max_x = 10;
  size_t max_y = 10;
  int32_t step_x = (width - 1) / max_x;
  int32_t step_y = (height - 1) / max_y;
  for (size_t i = 0; i <= max_x; ++i) {
    for (size_t j = 0; j <= max_y; ++j) {
      position ver{5 + static_cast<int>(i) * step_x,
                   5 + static_cast<int>(j) * step_y};
      vertexes_buffer.push_back(ver);
    }
  }
  std::vector<uint8_t> index_buffer;
  for (size_t x = 0; x < max_x; ++x) {
    for (size_t y = 0; y < max_y; ++y) {
      uint8_t index0 = static_cast<uint8_t>(y * (max_y + 1) + x);
      uint8_t index1 = static_cast<uint8_t>(index0 + (max_y + 1) + 1);
      uint8_t index2 = index1 - 1;
      uint8_t index3 = index0 + 1;

      index_buffer.push_back(index0);
      index_buffer.push_back(index2);
      index_buffer.push_back(index2);

      index_buffer.push_back(index0);
      index_buffer.push_back(index3);
      index_buffer.push_back(index3);

      index_buffer.push_back(index3);
      index_buffer.push_back(index1);
      index_buffer.push_back(index1);

      index_buffer.push_back(index1);
      index_buffer.push_back(index2);
      index_buffer.push_back(index2);
    }
  }
  trian_i_render.draw_triangle(vertexes_buffer, index_buffer, green);
  ib_triangles.save_image("triangles_indexes.ppm");
  return EXIT_SUCCESS;
}
