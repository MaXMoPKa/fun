#include "00_image_basic.h"
#include <iostream>

int main(int argc, char **argv, char **env) {
  const color green = {0, 255, 0};

  image image_saved;

  const char *image_name = "00_green_image.ppm";

  std::fill(begin(image_saved), end(image_saved), green);

  image_saved.save_image(image_name);

  image image_loaded;
  image_loaded.load_image(image_name);

  if (image_saved != image_loaded) {
    std::cerr << " image_saved != image_loaded";
    return EXIT_FAILURE;
  } else {
    std::cout << "image_saved == image_loaded";
  }

  return EXIT_SUCCESS;
}
