#pragma once

#include <array>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <fstream>
#include <string>
#include <vector>

// I set the height and the width and count the area of rhe image.
const size_t width = 320;
const size_t height = 240;
const size_t buffer_size = width * height;

struct color {
  uint8_t r = 0; // The red color.
  uint8_t g = 0; // The green color.
  uint8_t b = 0; // The blue color.
  friend bool operator==(const color &l, const color &r);
};

class image : public std::array<color, buffer_size> {
public:
  void save_image(const std::string &name_image) {
    std::ofstream out_file;
    out_file.exceptions(std::ios_base::failbit);
    out_file.open(name_image, std::ios_base::binary);
    out_file << "P6\n" << width << ' ' << height << ' ' << 255 << '\n';
    std::streamsize size_im =
        static_cast<std::streamsize>(sizeof(color) * size());
    out_file.write(reinterpret_cast<const char *>(this), size_im);
  }
  void load_image(const std::string &name_image) {
    std::ifstream in_file;
    in_file.exceptions(std::ios_base::failbit);
    in_file.open(name_image, std::ios_base::binary);
    std::string title;
    size_t image_width = 0;
    size_t image_height = 0;
    std::string color_type;
    in_file >> title >> image_width >> image_height >> color_type >> std::ws;
    if (size() != image_height * image_width) {
      throw std::runtime_error("Size of image is not match!");
    }
    std::streamsize size_im =
        static_cast<std::streamsize>(sizeof(color) * size());
    in_file.read(reinterpret_cast<char *>(this), size_im);
  }
};

struct position {
  double lenght() { return std::sqrt(x * x + y * y); }
  friend position operator-(const position &left, const position &right);
  friend bool operator==(const position &left, const position &right);
  int32_t x = 0;
  int32_t y = 0;
};

using pixels = std::vector<position>;

struct render {
  virtual void draw_pixel(position, color) = 0;
  virtual void clear(color) = 0;
  virtual pixels pixels_positions(position, position) = 0;

  virtual ~render();
};
