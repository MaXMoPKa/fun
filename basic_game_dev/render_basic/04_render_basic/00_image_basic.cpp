#include "00_image_basic.h"

render::~render() {}

bool operator==(const color &l, const color &r) {
  return l.r == r.r && l.g == r.g && l.b == r.b;
}

position operator-(const position &left, const position &right) {
  return {left.x - right.x, left.y - right.y};
}
bool operator==(const position &left, const position &right) {
  return left.x == right.x && left.y == right.y;
}
