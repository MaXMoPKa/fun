#include "03_index_buffer_and_vertex_buffer.h"

triangle_index_render::triangle_index_render(image &buffer, size_t width,
                                             size_t height)
    : triangle_render(buffer, width, height) {}

void triangle_index_render::draw_triangle(std::vector<position> &vertexes,
                                          std::vector<uint8_t> &indexes,
                                          color c) {
  pixels triangle_pixels;
  for (size_t i = 0; i < indexes.size() / 3; ++i) {
    uint8_t index0 = indexes[i * 3 + 0];
    uint8_t index1 = indexes[i * 3 + 1];
    uint8_t index2 = indexes[i * 3 + 2];

    position ver0 = vertexes.at(index0);
    position ver1 = vertexes.at(index1);
    position ver2 = vertexes.at(index2);
    for (auto pixels_position : pixels_position(ver0, ver1, ver2)) {
      triangle_pixels.push_back(pixels_position);
    }
  }
  for (auto pos : triangle_pixels) {
    draw_pixel(pos, c);
  }
}
