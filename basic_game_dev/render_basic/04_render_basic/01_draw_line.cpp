#include "01_draw_line.h"

#include <algorithm>
#include <cstdlib>

line_render::line_render(image &buffer_, size_t width, size_t height)
    : buffer(buffer_), w(width), h(height) {}

void line_render::clear(color c) { std::fill(begin(buffer), end(buffer), c); }

void line_render::draw_pixel(position p, color c) {
  const size_t i = static_cast<unsigned>(p.x) + static_cast<unsigned>(p.y) * w;
  if (i >= buffer.size()) {
    return;
  }
  color &col = buffer[i];
  col = c;
}

pixels line_render::pixels_positions(position start, position end) {
  pixels result;

  int x0 = start.x;
  int y0 = start.y;
  int x1 = end.x;
  int y1 = end.y;

  auto plot_line_low = [&](int x0, int y0, int x1, int y1) {
    int dx = abs(x1 - x0);
    int dy = abs(y1 - y0);
    int error = 0;
    int derr = (dy + 1);
    int y = y0;
    int diry = y1 - y0;
    if (diry > 0) {
      diry = 1;
    } else if (diry < 0) {
      diry = -1;
    }
    for (int x = x0; x <= x1; ++x) {
      result.push_back(position{x, y});
      error += derr;
      if (error >= (dx + 1)) {
        y = y + diry;
        error -= (dx + 1);
      }
    }
  };
  auto plot_line_height = [&](int x0, int y0, int x1, int y1) {
    int dx = abs(x1 - x0);
    int dy = abs(y1 - y0);
    int error = 0;
    int derr = (dx + 1);
    int x = x0;
    int dirx = x1 - x0;
    if (dirx > 0) {
      dirx = 1;
    } else if (dirx < 0) {
      dirx = -1;
    }
    for (int y = y0; y <= y1; ++y) {
      result.push_back(position{x, y});
      error += derr;
      if (error >= (dy + 1)) {
        x = x + dirx;
        error -= (dy + 1);
      }
    }
  };
  if (abs(y1 - y0) < abs(x1 - x0)) {
    if (x0 > x1) {
      plot_line_low(x1, y1, x0, y0);
    } else {
      plot_line_low(x0, y0, x1, y1);
    }
  } else {
    if (y0 > y1) {
      plot_line_height(x1, y1, x0, y0);
    } else {
      plot_line_height(x0, y0, x1, y1);
    }
  }
  return result;
}

void line_render::draw_line(position start, position end, color c) {
  pixels all_pos = pixels_positions(start, end);
  std::for_each(begin(all_pos), std::end(all_pos),
                [&](auto &pos) { draw_pixel(pos, c); });
}
