#pragma once

#include "01_draw_line.h"

struct triangle_render : line_render {
  triangle_render(image &buffer, size_t width, size_t height);
  virtual pixels pixels_position(position ver0, position ver1, position ver2);
  void draw_triangle(std::vector<position> &vertexes, size_t num_vertexes,
                     color c);
};
