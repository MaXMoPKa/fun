#pragma once

#include "00_image_basic.h"

struct line_render : render {
public:
  line_render(image &, size_t, size_t);

  void draw_pixel(position, color) override;
  void clear(color) override;
  pixels pixels_positions(position, position) override;
  void draw_line(position start, position end, color c);

private:
  image &buffer;
  const size_t w;
  const size_t h;
};
