#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>

#include "engine.h"

void play_triangles_in_line(const int w_start, const int h_start,
                            const int h_end,
                            std::vector<engine::triangle> &tr_vector);

void search_triangles_of_pause(
    std::vector<engine::triangle> &index_buffer_of_pause);

void search_triangles_of_play_1(
    std::vector<engine::triangle> &index_buffer_of_play);

void search_triangles_of_play_2(
    std::vector<engine::triangle> &triangles_buffer_of_play);

engine::vertex blend_vertex(const engine::vertex &vl, const engine::vertex &vr,
                            const float a) {
  engine::vertex r;
  r.x = (1.0f - a) * vl.x + a * vr.x;
  r.y = (1.0f - a) * vl.y + a * vr.y;
  return r;
}

engine::triangle blend(const engine::triangle &tl, const engine::triangle &tr,
                       const float a) {
  engine::triangle r;
  r.v[0] = blend_vertex(tl.v[0], tr.v[0], a);
  r.v[1] = blend_vertex(tl.v[1], tr.v[1], a);
  r.v[2] = blend_vertex(tl.v[2], tr.v[2], a);
  return r;
}

int main(int, char **, char **) {
  using namespace std;

  std::unique_ptr<engine::engine, void (*)(engine::engine *)> engine(
      engine::create_engine(), engine::destroy_engine);

  engine->initialize("");

  bool continue_loop = true;
  while (continue_loop) {
    engine::event event;
    while (engine->read_inrut(event)) {
      cout << event << endl;
      switch (event) {
      case engine::event::turn_off:
        continue_loop = false;
        break;
      default:
        break;
      }
    }
    std::ifstream file_play("vertexes_of_play.txt");
    assert(!!file_play);
    std::ifstream file_pause("vertexes_of_pause.txt");
    assert(!!file_pause);

    std::vector<engine::triangle> tr_play;
    std::vector<engine::triangle> tr_pause;
    engine::triangle triangle_from_file;

    while (!file_play.eof()) {
      file_play >> triangle_from_file;
      tr_play.push_back(triangle_from_file);
    }
    //    engine->render_triangle(tr_play);

    while (!file_pause.eof()) {
      file_pause >> triangle_from_file;
      tr_pause.push_back(triangle_from_file);
    }
    //    engine->render_triangle(tr_pause);

    std::vector<engine::triangle> tr;

    for (size_t i = 0; i < tr_play.size(); ++i) {
      engine::triangle tr_from_play;
      engine::triangle tr_from_pause;
      engine::triangle tr_result;

      tr_from_play = tr_play.at(i);
      tr_from_pause = tr_pause.at(i);
      float alpha = (std::sin(engine->get_time_from_init()) * 0.5f) + 0.5f;
      tr_result = blend(tr_from_play, tr_from_pause, alpha);
      tr.push_back(tr_result);
    }
    engine->render_triangle(tr);

    engine->swap_buffers();
  }
  engine->uninitialize();
  return EXIT_SUCCESS;
}
