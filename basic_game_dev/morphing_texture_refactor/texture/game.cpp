#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>

#include "engine.h"

engine::vertex blend_vertex(const engine::vertex &vl, const engine::vertex &vr,
                            const float a) {
  engine::vertex r;
  r.x = (1.0f - a) * vl.x + a * vr.x;
  r.y = (1.0f - a) * vl.y + a * vr.y;
  return r;
}

engine::triangle blend(const engine::triangle &tl, const engine::triangle &tr,
                       const float a) {
  engine::triangle r;
  r.v[0] = blend_vertex(tl.v[0], tr.v[0], a);
  r.v[1] = blend_vertex(tl.v[1], tr.v[1], a);
  r.v[2] = blend_vertex(tl.v[2], tr.v[2], a);
  return r;
}

int main(int, char **, char **) {
  using namespace std;

  std::unique_ptr<engine::engine, void (*)(engine::engine *)> engine(
      engine::create_engine(), engine::destroy_engine);

  const std::string error = engine->initialize("");
  if (!error.empty()) {
    std::cerr << error << std::endl;
    return EXIT_FAILURE;
  }

  bool continue_loop = true;
  while (continue_loop) {
    engine::event event;
    while (engine->read_inrut(event)) {
      cout << event << endl;
      switch (event) {
      case engine::event::turn_off:
        continue_loop = false;
        break;
      default:
        break;
      }
    }
    std::ifstream file("vert_and_text_coord.txt");
    assert(!!file);

    engine::triangle tr1;
    engine::triangle tr2;

    file >> tr1 >> tr2;

    engine->render_triangle(tr1);
    engine->render_triangle(tr2);

    engine->swap_buffers();
  }
  engine->uninitialize();
  return EXIT_SUCCESS;
}
