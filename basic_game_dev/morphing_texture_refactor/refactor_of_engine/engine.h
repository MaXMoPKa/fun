#include <iosfwd>
#include <string>
#include <string_view>

#ifndef ENGINE_DECLSPEC
#define ENGINE_DECLSPEC
#endif

namespace engine {
enum class event {
  up_pressed,
  up_released,
  left_pressed,
  left_released,
  dowm_pressed,
  down_released,
  right_pressed,
  right_released,
  fire_pressed,
  fire_released,
  pause_pressed,
  pause_released,
  play_pressed,
  play_released,
  turn_off
};
ENGINE_DECLSPEC std::ostream &operator<<(std::ostream &stream, const event e);

class engine;

ENGINE_DECLSPEC engine *create_engine();
ENGINE_DECLSPEC void destroy_engine(engine *eng);

class color {
public:
  color() = default;
  explicit color(std::uint32_t rgba_);
  color(float r, float g, float b, float a);

  float get_r() const;
  float get_g() const;
  float get_b() const;
  float get_a() const;

  void set_r(const float r);
  void set_g(const float g);
  void set_b(const float b);
  void set_a(const float a);

private:
  std::uint32_t rgba = 0;
};

// pos in 2d sapce
struct pos {
  float x = 0.f;
  float y = 0.f;
};

// texture pos
struct uv_pos {
  float u = 0.f;
  float v = 0.f;
};

// vertex with pos only
struct v0 {
  pos p;
};

// vertex with pos and color
struct v1 {
  pos p;
  color c;
};

// vertex with pos and texture pos and color
struct v2 {
  pos p;
  uv_pos uv;
  color c;
};

// triangle with pos only
struct tr0 {
  tr0();
  v0 v[3];
};

// triangle with pos and color
struct tr1 {
  tr1();
  v1 v[3];
};

// triangle with pos, color and texture pos
struct tr2 {
  tr2();
  v2 v[3];
};

std::istream &operator>>(std::istream &stream, uv_pos &);
std::istream &operator>>(std::istream &stream, color &);
std::istream &operator>>(std::istream &stream, v0 &);
std::istream &operator>>(std::istream &stream, v1 &);
std::istream &operator>>(std::istream &stream, v2 &);
std::istream &operator>>(std::istream &stream, tr0 &);
std::istream &operator>>(std::istream &stream, tr1 &);
std::istream &operator>>(std::istream &stream, tr2 &);

class texture {
public:
  virtual ~texture();
  virtual std::uint32_t get_width() const = 0;
  virtual std::uint32_t get_height() const = 0;
};

class ENGINE_DECLSPEC engine {
public:
  virtual ~engine();
  // create main window
  virtual std::string initialize(std::string_view config) = 0;
  // return count of seconds after initialize
  virtual float get_time_from_init() = 0;
  virtual bool read_inrut(event &e) = 0;
  virtual texture *create_texture(std::string_view path) = 0;
  virtual void destroy_texture(texture *t) = 0;
  virtual void render(const tr0 &, const color &) = 0;
  virtual void render(const tr1 &) = 0;
  virtual void render(const tr2 &, texture *) = 0;
  virtual void swap_buffers() = 0;
  virtual void uninitialize() = 0;
};

} // namespace engine
