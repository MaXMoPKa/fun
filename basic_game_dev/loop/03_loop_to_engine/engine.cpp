#include <algorithm>
//#include <array>
//#include <cstdint>
//#include <cstdlib>
//#include <exception>
//#include <iostream>
#include <sstream>
//#include <stdexcept>
//#include <vector>

#include "engine.h"
#include <SDL.h>
namespace engine {
static std::array<std::string_view, 15> event_names{
    {"UP_pressed", "UP_released", "LEFT_pressed", "LEFT_released",
     "DOWN_pressed", "DOWN_released", "RIGHT_pressed", "RIGHT_released",
     "FIRE_pressed", "FIRE_released", "PAUSE_pressed", "PAUSE_released",
     "PLAY_pressed", "PLAY_released", "TURN_off"}};
std::ostream &operator<<(std::ostream &stream, const event e) {
  std::uint32_t value = static_cast<std::uint32_t>(e);
  std::uint32_t minimal = static_cast<std::uint32_t>(event::up_pressed);
  std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
  if (value >= minimal && value <= maximal) {
    stream << event_names[value];
    return stream;
  } else {
    throw std::runtime_error("Too big event value");
  }
}
struct bind {
  SDL_Keycode key;
  std::string_view name;
  event event_pressed;
  event event_released;
};

const std::array<bind, 8> keys{
    {{SDLK_w, "UP", event::up_pressed, event::up_released},
     {SDLK_a, "LEFT", event::left_pressed, event::left_released},
     {SDLK_s, "DOWN", event::dowm_pressed, event::down_released},
     {SDLK_d, "RIGHT", event::right_pressed, event::right_released},
     {SDLK_SPACE, "FIRE", event::fire_pressed, event::fire_released},
     {SDLK_ESCAPE, "PAUSE", event::pause_pressed, event::pause_released},
     {SDLK_KP_ENTER, "PLAY", event::play_pressed, event::play_released}}};
static bool check_input(const SDL_Event &e, const bind *&result) {
  using namespace std;
  const auto it = find_if(begin(keys), end(keys), [&](const bind &b) {
    return b.key == e.key.keysym.sym;
  });
  if (it != end(keys)) {
    result = &(*it);
    return true;
  }
  return false;
}
class engine_impl final : public engine {
public:
  std::string initialize(std::string_view) final {
    using namespace std;
    stringstream serr;
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
      serr << "Unable to initialize SDL: " << SDL_GetError() << endl;
      return serr.str();
    }
    SDL_Window *window;
    window = SDL_CreateWindow("An SDL2 window", SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, 640, 480,
                              SDL_WINDOW_OPENGL | SDL_WINDOW_MOUSE_FOCUS);
    if (window == nullptr) {
      serr << "Could not create window: " << SDL_GetError() << endl;
      SDL_Quit();
      return serr.str();
    }
    return "";
  }
  bool read_inrut(event &e) final {
    using namespace std;
    SDL_Event sdl_event;
    if (SDL_PollEvent(&sdl_event)) {
      const bind *binding = nullptr;
      if (sdl_event.type == SDL_QUIT) {
        e = event::turn_off;
        return true;
      } else if (sdl_event.type == SDL_KEYDOWN) {
        if (check_input(sdl_event, binding)) {
          e = binding->event_pressed;
          return true;
        }
      } else if (sdl_event.type == SDL_KEYUP) {
        if (check_input(sdl_event, binding)) {
          e = binding->event_released;
          return true;
        }
      }
    }
    return false;
  }
  void uninitialize() final {}
};
static bool already_exist = false;
engine *create_engine() {
  if (already_exist) {
    throw std::runtime_error("Engine already exist");
  }
  engine *result = new engine_impl();
  already_exist = true;
  return result;
}
void destroy_engine(engine *eng) {
  if (already_exist == false) {
    throw std::runtime_error("Engine didn't create");
  }
  if (eng == nullptr) {
    throw std::runtime_error("eng is nullptr");
  }
  delete eng;
}
engine::~engine(){};
} // namespace engine
