//#include <iosfwd>
#include <string>
#include <string_view>

namespace engine {
enum class event {
  up_pressed,
  up_released,
  left_pressed,
  left_released,
  dowm_pressed,
  down_released,
  right_pressed,
  right_released,
  fire_pressed,
  fire_released,
  pause_pressed,
  pause_released,
  play_pressed,
  play_released,
  turn_off
};
std::ostream &operator<<(std::ostream &stream, const event e);

class engine;

engine *create_engine();
void destroy_engine(engine *eng);

class engine {
public:
  virtual ~engine();
  virtual std::string initialize(std::string_view config) = 0;
  virtual bool read_inrut(event &e) = 0;
  virtual void uninitialize() = 0;
};

} // namespace engine
