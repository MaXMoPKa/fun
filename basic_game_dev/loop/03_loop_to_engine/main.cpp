#include <iostream>
#include <memory>

#include "engine.h"

int main(int, char **, char **) {
  using namespace std;
  unique_ptr<engine::engine, void (*)(engine::engine *)> engine(
      engine::create_engine(), engine::destroy_engine);
  string err = engine->initialize("");
  if (!err.empty()) {
    cerr << err << endl;
    return EXIT_FAILURE;
  }
  bool continue_loop = true;
  while (continue_loop) {
    engine::event event;
    while (engine->read_inrut(event)) {
      cout << event << endl;
      switch (event) {
      case engine::event::turn_off:
        continue_loop = false;
        break;
      default:
        break;
      }
    }
  }
  engine->uninitialize();
  return EXIT_SUCCESS;
}
