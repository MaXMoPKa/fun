#include <algorithm>
#include <array>
#include <cstdlib>
#include <iostream>
#include <string_view>

#include <SDL.h>

struct keyboard {
  SDL_KeyCode key;
  std::string_view name;
};

// bool somethings(const SDL_Event &e, const array<> /*const keyboard &b*/) {
//  return b.key == e.key.keysym.sym;
//}

void check_input(const SDL_Event &e) {
  using namespace std;
  const array<keyboard, 8> keys = {{
      {SDLK_w, "UP"},
      {SDLK_a, "LEFT"},
      {SDLK_s, "DOWN"},
      {SDLK_d, "RIGHT"},
      {SDLK_SPACE, "FIRE"},
      {SDLK_ESCAPE, "PAUSE"},

  }};
  const auto it = find_if(begin(keys), end(keys), [&](const keyboard &b) {
    return b.key == e.key.keysym.sym;
  });
  if (it != end(keys)) {
    cout << it->name << ' ';
    if (e.type == SDL_KEYDOWN) {
      cout << "is pressed" << endl;
    } else {
      cout << "is released" << endl;
    }
  }
}

int main(int /*argc*/, char ** /*argv*/, char ** /*env*/) {
  using namespace std;
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
    return EXIT_FAILURE;
  }
  SDL_Window *window;
  window = SDL_CreateWindow("An SDL2 window", SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 640, 480,
                            SDL_WINDOW_OPENGL | SDL_WINDOW_MOUSE_FOCUS);
  if (window == nullptr) {
    SDL_Log("Could not create window: %s\n", SDL_GetError());
    SDL_Quit();
    return EXIT_FAILURE;
  }
  bool control_ivent = true;
  while (control_ivent) {
    SDL_Event test_event;
    while (SDL_PollEvent(&test_event)) {
      switch (test_event.type) {
      case SDL_KEYDOWN:
        check_input(test_event);
        break;
      case SDL_KEYUP:
        check_input(test_event);
        break;
      case SDL_QUIT:
        control_ivent = false;
        break;
      default:
        break;
      }
    }
  }
  SDL_DestroyWindow(window);
  SDL_Quit();
  return EXIT_SUCCESS;
}
