#include <iosfwd>
#include <string>
#include <string_view>

#ifndef ENGINE_DECLSPEC
#define ENGINE_DECLSPEC
#endif

namespace engine {
enum class event {
  up_pressed,
  up_released,
  left_pressed,
  left_released,
  dowm_pressed,
  down_released,
  right_pressed,
  right_released,
  fire_pressed,
  fire_released,
  pause_pressed,
  pause_released,
  play_pressed,
  play_released,
  turn_off
};
ENGINE_DECLSPEC std::ostream &operator<<(std::ostream &stream, const event e);

class engine;

ENGINE_DECLSPEC engine *create_engine();
ENGINE_DECLSPEC void destroy_engine(engine *eng);

class ENGINE_DECLSPEC engine {
public:
  virtual ~engine();
  virtual void uninitialize() = 0;
  virtual std::string initialize(std::string_view config) = 0;
  virtual bool read_inrut(event &e) = 0;
};

} // namespace engine
