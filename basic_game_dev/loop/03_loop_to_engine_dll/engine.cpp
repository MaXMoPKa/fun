#include "engine.h"

#include <algorithm>
#include <array>
#include <cstdint>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <SDL.h>
namespace engine {
static std::array<std::string_view, 15> event_names{
    {"UP_pressed", "UP_released", "LEFT_pressed", "LEFT_released",
     "DOWN_pressed", "DOWN_released", "RIGHT_pressed", "RIGHT_released",
     "FIRE_pressed", "FIRE_released", "PAUSE_pressed", "PAUSE_released",
     "PLAY_pressed", "PLAY_released", "TURN_off"}};
std::ostream &operator<<(std::ostream &stream, const event e) {
  std::uint32_t value = static_cast<std::uint32_t>(e);
  std::uint32_t minimal = static_cast<std::uint32_t>(event::up_pressed);
  std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
  if (value >= minimal && value <= maximal) {
    stream << event_names[value];
    return stream;
  } else {
    throw std::runtime_error("Too big event value");
  }
}
struct bind_keyboard {
  SDL_Keycode key;
  std::string_view name;
  event event_pressed;
  event event_released;
};

const std::array<bind_keyboard, 8> keys{
    {{SDLK_w, "UP", event::up_pressed, event::up_released},
     {SDLK_a, "LEFT", event::left_pressed, event::left_released},
     {SDLK_s, "DOWN", event::dowm_pressed, event::down_released},
     {SDLK_d, "RIGHT", event::right_pressed, event::right_released},
     {SDLK_SPACE, "FIRE", event::fire_pressed, event::fire_released},
     {SDLK_ESCAPE, "PAUSE", event::pause_pressed, event::pause_released},
     {SDLK_KP_ENTER, "PLAY", event::play_pressed, event::play_released}}};
static bool check_input_keyboard_event(const SDL_Event &e,
                                       const bind_keyboard *&result) {
  using namespace std;
  const auto it_for_keyboard =
      find_if(begin(keys), end(keys), [&](const bind_keyboard &b) {
        return b.key == e.key.keysym.sym;
      });
  if (it_for_keyboard != end(keys)) {
    result = &(*it_for_keyboard);
    return true;
  }
  return false;
}

struct bind_controller {
  SDL_GameControllerButton button;
  std::string_view name;
  event event_pressed;
  event event_released;
};

const std::array<bind_controller, 8> controller_buttons{
    {{SDL_CONTROLLER_BUTTON_DPAD_UP, "UP", event::up_pressed,
      event::up_released},
     {SDL_CONTROLLER_BUTTON_DPAD_LEFT, "LEFT", event::left_pressed,
      event::left_released},
     {SDL_CONTROLLER_BUTTON_DPAD_DOWN, "DOWN", event::dowm_pressed,
      event::down_released},
     {SDL_CONTROLLER_BUTTON_DPAD_RIGHT, "RIGHT", event::right_pressed,
      event::right_released},
     {SDL_CONTROLLER_BUTTON_X, "FIRE", event::fire_pressed,
      event::fire_released},
     {SDL_CONTROLLER_BUTTON_A, "PAUSE", event::pause_pressed,
      event::pause_released},
     {SDL_CONTROLLER_BUTTON_B, "PLAY", event::play_pressed,
      event::play_released}}};
static bool check_input_controller_event(const SDL_Event &e,
                                         const bind_controller *&result) {
  using namespace std;
  const auto it_for_controller = find_if(
      begin(controller_buttons), end(controller_buttons),
      [&](const bind_controller &b) { return b.button == e.cbutton.state; });
  if (it_for_controller != end(controller_buttons)) {
    result = &(*it_for_controller);
    return true;
  }
  return false;
}

class engine_impl final : public engine {
public:
  std::string initialize(std::string_view) final {
    using namespace std;
    stringstream serr;
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
      serr << "Unable to initialize SDL: " << SDL_GetError() << endl;
      return serr.str();
    }
    SDL_Window *window;
    window = SDL_CreateWindow("An SDL2 window", SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, 640, 480,
                              SDL_WINDOW_OPENGL | SDL_WINDOW_MOUSE_FOCUS);
    if (window == nullptr) {
      serr << "Could not create window: " << SDL_GetError() << endl;
      SDL_Quit();
      return serr.str();
    }
    SDL_GameController *controller = nullptr;
    for (int i = 0; i < SDL_NumJoysticks(); ++i) {
      if (SDL_IsGameController(i)) {
        controller = SDL_GameControllerOpen(i);
        if (controller) {
          break;
        } else {
          fprintf(stderr, "Could not open gamcontoller %i: %s\n", i,
                  SDL_GetError());
        }
      }
    }
    return "";
  }
  bool read_inrut(event &e) final {
    using namespace std;
    SDL_Event sdl_event;
    if (SDL_PollEvent(&sdl_event)) {
      const bind_keyboard *binding_keyboard = nullptr;
      const bind_controller *binding_controller = nullptr;
      if (sdl_event.type == SDL_QUIT) {
        e = event::turn_off;
        return true;
      } else if (sdl_event.type == SDL_KEYDOWN) {
        if (check_input_keyboard_event(sdl_event, binding_keyboard)) {
          e = binding_keyboard->event_pressed;
          return true;
        }
      } else if (sdl_event.type == SDL_KEYUP) {
        if (check_input_keyboard_event(sdl_event, binding_keyboard)) {
          e = binding_keyboard->event_released;
          return true;
        }
      } else if (sdl_event.type == SDL_CONTROLLERDEVICEADDED) {
        cerr << "Controller added" << endl;
      } else if (sdl_event.type == SDL_CONTROLLERDEVICEREMOVED) {
        cerr << "Controller removed" << endl;
      } else if (sdl_event.type == SDL_CONTROLLERBUTTONDOWN) {
        if (check_input_controller_event(sdl_event, binding_controller)) {
          e = binding_controller->event_pressed;
          return true;
        }
      } else if (sdl_event.type == SDL_CONTROLLERBUTTONUP) {
        if (check_input_controller_event(sdl_event, binding_controller)) {
          e = binding_controller->event_released;
          return true;
        }
      }
    }
    return false;
  }
  void uninitialize() final {}
};
static bool already_exist = false;
engine *create_engine() {
  if (already_exist) {
    throw std::runtime_error("Engine already exist");
  }
  engine *result = new engine_impl();
  already_exist = true;
  return result;
}
void destroy_engine(engine *eng) {
  if (already_exist == false) {
    throw std::runtime_error("Engine didn't create");
  }
  if (eng == nullptr) {
    throw std::runtime_error("eng is nullptr");
  }
  delete eng;
}
engine::~engine(){};
} // namespace engine
