FROM archlinux:latest

MAINTAINER Vladislav Aleynikov <vladislav_v_01@mail.ru>

RUN pacman -Syu --noconfirm git gcc cmake make ninja sdl2

ENV CMAKE_GENERATOR "Ninja"

COPY . .

CMD ./scripts/run.sh
